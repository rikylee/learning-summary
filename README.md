# Learning-Summary

项目、学习知识点整理汇总

## 计算机基础

### 网络

- [网络模型](docs/网络/Network_Model.md)
- TCP
- UDP
- HTTP
- [HTTP 状态码及其含义](docs/网络/HttpCode.md)
- [从输入 URL 到页面展示全过程](docs/网络/Url.md)

### 操作系统

### 数据结构

- [数组](docs/数据结构/数组.md)
- [链表](docs/数据结构/链表.md)
- [栈和队列](docs/数据结构/栈和队列.md)
- [树-二叉树](docs/数据结构/树-二叉搜索树.md)
- [树-平衡二叉树](docs/数据结构/树-平衡二叉树.md)
- [树-红黑树](docs/数据结构/树-红黑树.md)
- [堆](docs/数据结构/堆.md)
- 图

### 算法

- 递归
- [排序](docs/算法/排序.md)
- 动态规划
- Top N
- 布隆过滤器
- BitMap
- LRU 缓存
- 复杂度分析

### 数据库

- [MySQL Group Replication 高可用集群](docs/数据库/mysql-group-replication.md)
- Redis
- MongoDB

## Java

### Java 基础

### 集合框架

### 并发

### JVM

- [常用的 JVM 参数](docs/java/Jvm_Parameters.md)
- 类加载
- 垃圾回收
- 内存模型
- 性能问题

### Spring

- Spring MVC
- Spring Boot
  - [基于 Spring Boot 实现简单的反向代理功能](/docs/spring/boot/基于 Spring Boot 实现简单的反向代理功能.md)
- Spring Cloud

### Netty

## 分布式

- CAP
- API 网关
- Zk
- ES
- Solr
- Raft 算法
- [分布式 ID](docs/分布式/distributed_id.md)

## 消息队列

- [RocketMQ 发送延时消息](docs/消息队列/rocketmq-delay-message.md)

### RokcetMQ

### Kafka

## 认证授权

### SSO、JWT

## 设计模式

## 架构设计

## 日志、测试

## 安全防范

## DevOps

### Docker

### K8S

- [k8s 证书](docs/k8s/certificate-expired.md)
- [k8s 节点加入异常](docs/k8s/add-new-control-plane.md)
- [ingress nginx 使用 Godaddy SSL 证书](docs/k8s/ingress-nginx-ssl.md)
- [k8s 集群升级](docs/k8s/k8s-cluster-upgrade.md)
- [cert-manager Let's Encrypt 实现 Nginx ingress TLS 自动证书签发](docs/k8s/cert-manager_TLS.md)

### 运维与监控

- [监控概述](docs/运维与监控/monitor.md)
- [基于 Prometheus Grafana 的监控环境](docs/运维与监控监控/prometheus-grafana-monitor.md)
- [Linux TC 流量控制](docs/运维与监控/linux-tc.md)
- [使用 Linux TC 进行流量限制](docs/运维与监控/使用Linux-TC进行流量限制.md)
- [基于 named DNS 服务器搭建与配置](docs/运维与监控/named-dns-server.md)
- [基于阿里云解析实现 DDNS 内网服务穿透](docs/运维与监控/ddns.md)
- [Ubuntu 20.04 Samba 使用 Active Directory 登录认证](docs/运维与监控/ubuntu-samba-ad.md)

## 中间件

### Nginx

- [yum 安装下的 nginx，重新编辑安装添加模块和添加第三方模块](docs/nginx/nginx-add-module.md)

### Apache HTTP

### Tomcat

## 前端

### Vue

### React
