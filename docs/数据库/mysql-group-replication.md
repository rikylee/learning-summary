# Mysql group replication 高可用集群搭建

## 服务器信息

- share01
  - IP 10.3.175.60
  - OS CentOS Linux release 7.9.2009
- share02
  - IP 10.3.175.61
  - OS CentOS Linux release 7.9.2009
- share03
  - IP 10.3.175.62
  - OS CentOS Linux release 7.9.2009

## 安装 wget

```shell
yum install wget -y
```

## 替换为阿里 centos 镜像源

```shell
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
```

## 替换为阿里 epel 镜像源

```shell
mv /etc/yum.repos.d/epel.repo /etc/yum.repos.d/epel.repo.backup
wget -O /etc/yum.repos.d/epel.repo http://mirrors.cloud.tencent.com/repo/epel-7.repo
```

## 添加清华大学 mysql 镜像源

```shell
cat>>/etc/yum.repos.d/mysql-community.repo << \EOF
[mysql-connectors-community]
name=MySQL Connectors Community
baseurl=https://mirrors.tuna.tsinghua.edu.cn/mysql/yum/mysql-connectors-community-el7-$basearch/
enabled=1
gpgcheck=1
gpgkey=https://repo.mysql.com/RPM-GPG-KEY-mysql

[mysql-tools-community]
name=MySQL Tools Community
baseurl=https://mirrors.tuna.tsinghua.edu.cn/mysql/yum/mysql-tools-community-el7-$basearch/
enabled=1
gpgcheck=1
gpgkey=https://repo.mysql.com/RPM-GPG-KEY-mysql

[mysql-5.6-community]
name=MySQL 5.6 Community Server
baseurl=https://mirrors.tuna.tsinghua.edu.cn/mysql/yum/mysql-5.6-community-el7-$basearch/
enabled=0
gpgcheck=1
gpgkey=https://repo.mysql.com/RPM-GPG-KEY-mysql

[mysql-5.7-community]
name=MySQL 5.7 Community Server
baseurl=https://mirrors.tuna.tsinghua.edu.cn/mysql/yum/mysql-5.7-community-el7-$basearch/
enabled=1
gpgcheck=1
gpgkey=https://repo.mysql.com/RPM-GPG-KEY-mysql

[mysql-8.0-community]
name=MySQL 8.0 Community Server
baseurl=https://mirrors.tuna.tsinghua.edu.cn/mysql/yum/mysql-8.0-community-el7-$basearch/
enabled=1
gpgcheck=1
gpgkey=https://repo.mysql.com/RPM-GPG-KEY-mysql

EOF
```

## 更新 centos repo

```shell
yum clean all && yum makecache && yum update -y
```

## 安装 ntp

```shell
yum install ntp -y && systemctl enable --now ntpd
```

## 安装 mysql-shell mysql-community-server mysql-router

```shell
yum install -y mysql-shell mysql-community-server mysql-router
```

## 禁用 selinux

```shell
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
```

## 关闭防火墙 firewalld（可选，或者自行开放端口）

```shell
systemctl stop firewalld  && systemctl disable firewalld
```

## 系统内核参数优化

```shell
cat>>/etc/sysctl.conf <<EOF
fs.aio-max-nr = 1048576
fs.file-max = 681574400
kernel.shmmax = 137438953472
kernel.shmmni = 4096
kernel.sem = 250 32000 100 200
net.ipv4.ip_local_port_range = 9000 65000
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048586
net.ipv4.ip_forward = 1
EOF

sysctl -p
```

## 设置文件句柄

```shell
cat>>/etc/security/limits.conf <<EOF
mysql soft nproc 65536
mysql hard nproc 65536
mysql soft nofile 65536
mysql hard nofile 65536
EOF
```

## 重启服务器

```shell
reboot
```

## 修改 hosts 文件，或者配置 dns

```shell
cat>>/etc/hosts <<EOF
10.3.175.60 share01
10.3.175.61 share02
10.3.175.62 share03
EOF
```

**_NOTE: All of three nodes should be excute these command_**

## 启动 MySQL 实例

```shell
[root@share01 ~]# systemctl start mysqld
[root@share01 ~]# systemctl status mysqld
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-06-22 17:05:40 CST; 1 weeks 5 days ago
     Docs: man:mysqld(8)
           <http://dev.mysql.com/doc/refman/en/using-systemd.html>
 Main PID: 8668 (mysqld)
   Status: "Server is operational"
   CGroup: /system.slice/mysqld.service
           └─8668 /usr/sbin/mysqld


[root@share02 ~]# systemctl start mysqld
[root@share02 ~]# systemctl status mysqld
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-06-22 17:10:40 CST; 1 weeks 5 days ago
     Docs: man:mysqld(8)
           <http://dev.mysql.com/doc/refman/en/using-systemd.html>
 Main PID: 8668 (mysqld)
   Status: "Server is operational"
   CGroup: /system.slice/mysqld.service
           └─8668 /usr/sbin/mysqld


[root@share03 ~]# systemctl start mysqld
[root@share03 ~]# systemctl status mysqld
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-06-22 17:20:40 CST; 1 weeks 5 days ago
     Docs: man:mysqld(8)
           <http://dev.mysql.com/doc/refman/en/using-systemd.html>
 Main PID: 8668 (mysqld)
   Status: "Server is operational"
   CGroup: /system.slice/mysqld.service
           └─8668 /usr/sbin/mysqld
```

## 获取 MySQL 实例密码

```shell
[root@share01 ~]# grep 'temporary password' /var/log/mysqld.log
2021-06-22T02:31:06.548070Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: y)a<rI;-v11s

[root@share02 ~]# grep 'temporary password' /var/log/mysqld.log
2021-06-22T02:31:02.199568Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: BLiP2y.wqK+h

[root@share03 ~]# grep 'temporary password' /var/log/mysqld.log
2021-06-22T05:43:40.231455Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: Z:-inKduo4XP

```

## 修改密钥并授权

- share01

```shell
[root@share01 ~]# mysql -uroot -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 556
Server version: 8.0.25 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>use mysql;
mysql>update user set host='%' where user='root';
mysql>flush privileges;

mysql>grant all privileges on _._ to 'root'@'%' with grant option;
mysql>ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';
mysql>flush privileges;
mysql>exit

```

- share02

```shell
[root@share02 ~]# mysql -uroot -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 556
Server version: 8.0.25 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>use mysql;
mysql>update user set host='%' where user='root';
mysql>flush privileges;

mysql>grant all privileges on _._ to 'root'@'%' with grant option;
mysql>ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';
mysql>flush privileges;
mysql>exit
```

- share03

```shell
[root@share03 ~]# mysql -uroot -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 556
Server version: 8.0.25 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>use mysql;
mysql>update user set host='%' where user='root';
mysql>flush privileges;

mysql>grant all privileges on _._ to 'root'@'%' with grant option;
mysql>ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';
mysql>flush privileges;
mysql>exit

```

## 停止 MySQL 实例

```she
[root@share01 ~]# systemctl stop mysqld
[root@share02 ~]# systemctl stop mysqld
[root@share03 ~]# systemctl stop mysqld
```

## 编辑 `/etc/my.cnf`

- share01

```shell
[root@share01 ~]# vi /etc/my.cnf

# For advice on how to change settings please see
# http://dev.mysql.com/doc/refman/8.0/en/server-configuration-defaults.html

[mysqld]
#
# Remove leading # and set to the amount of RAM for the most important data
# cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
# innodb_buffer_pool_size = 128M
#
# Remove the leading "# " to disable binary logging
# Binary logging captures changes between backups and is enabled by
# default. It's default setting is log_bin=binlog
# disable_log_bin
#
# Remove leading # to set options mainly useful for reporting servers.
# The server defaults are faster for transactions and fast SELECTs.
# Adjust sizes as needed, experiment to find the optimal values.
# join_buffer_size = 128M
# sort_buffer_size = 2M
# read_rnd_buffer_size = 2M
#
# Remove leading # to revert to previous value for default_authentication_plugin,
# this will increase compatibility with older clients. For background, see:
# https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_default_authentication_plugin
# default-authentication-plugin=mysql_native_password

# 数据存放目录
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

# error日志存放目录
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

# 禁用除了innoDB存储引擎之外的存储
disabled_storage_engines=MyISAM,BLACKHOLE,FEDERATED,ARCHIVE,MEMORY

# server id 每台必须拥有不一样的id ，1~2^32 - 1
server_id=60

# 全局事务标识符打开
gtid_mode=ON

enforce_gtid_consistency=ON

# 禁用写入二进制日志的事件的校验和
binlog_checksum=NONE

log_bin=binlog
log_slave_updates=ON

# binlog 格式化为ROW
binlog_format=ROW

master_info_repository=TABLE

relay_log_info_repository=TABLE

# 设置最大连接数
max_connections=200

# 设置最大数据包的大小
max_allowed_packet=16M

# server必须为每个事务收集写集合，并使用XXHASH64哈希算法将其编码为散列
transaction_write_set_extraction=XXHASH64

# 告知插件加入或创建组命名，必须是有效的 UUID
loose-group_replication_group_name="7a774c9d-d328-11eb-a839-fcaa14c7ffe7"

# server启动时不自启组复制,为了避免每次启动自动引导具有相同名称的第二个组,所以设置为OFF
loose-group_replication_start_on_boot=off

# 告诉插件使用IP地址，端口33061用于接收组中其他成员转入连接
loose-group_replication_local_address= "10.3.175.60:33061"

# 设置组成员的主机名和端口，新成员使用它们来建立与组的连接
loose-group_replication_group_seeds= "10.3.175.60:33061,10.3.175.61:33061,10.3.175.62:33061"

# 指示插件是否引导组
loose-group_replication_bootstrap_group=off

# 配置单主模式
loose-group_replication_single_primary_mode=ON

# 设置复制组成员白名单
loose-group_replication_ip_whitelist="10.3.172.0/22"

loose-group_replication_enforce_update_everywhere_checks=off


```

- share02

```shell
[root@share02 ~]# vi /etc/my.cnf

# For advice on how to change settings please see
# http://dev.mysql.com/doc/refman/8.0/en/server-configuration-defaults.html
[mysqld]
#
# Remove leading # and set to the amount of RAM for the most important data
# cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
# innodb_buffer_pool_size = 128M
#
# Remove the leading "# " to disable binary logging
# Binary logging captures changes between backups and is enabled by
# default. It's default setting is log_bin=binlog
# disable_log_bin
#
# Remove leading # to set options mainly useful for reporting servers.
# The server defaults are faster for transactions and fast SELECTs.
# Adjust sizes as needed, experiment to find the optimal values.
# join_buffer_size = 128M
# sort_buffer_size = 2M
# read_rnd_buffer_size = 2M
#
# Remove leading # to revert to previous value for default_authentication_plugin,
# this will increase compatibility with older clients. For background, see:
# https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_default_authentication_plugin
# default-authentication-plugin=mysql_native_password

# 数据存放目录
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

# error日志存放目录
log-error=/var/log/mysqld.log

pid-file=/var/run/mysqld/mysqld.pid

# 禁用除了innoDB存储引擎之外的存储
disabled_storage_engines=MyISAM,BLACKHOLE,FEDERATED,ARCHIVE,MEMORY

# server id 每台必须拥有不一样的id ，1~2^32 - 1
server_id=61

# 全局事务标识符打开
gtid_mode=ON
enforce_gtid_consistency=ON

#  禁用写入二进制日志的事件的校验和
binlog_checksum=NONE

log_bin=binlog

log_slave_updates=ON

# binlog 格式化为ROW
binlog_format=ROW

master_info_repository=TABLE

relay_log_info_repository=TABLE

# 设置最大连接数
max_connections=200

# 设置最大数据包的大小
max_allowed_packet=16M

# server必须为每个事务收集写集合，并使用XXHASH64哈希算法将其编码为散列
transaction_write_set_extraction=XXHASH64

# 告知插件加入或创建组命名，必须是有效的 UUID
loose-group_replication_group_name="7a774c9d-d328-11eb-a839-fcaa14c7ffe7"

# server启动时不自启组复制,为了避免每次启动自动引导具有相同名称的第二个组,所以设置为OFF
loose-group_replication_start_on_boot=off

# 告诉插件使用IP地址，端口33061用于接收组中其他成员转入连接
loose-group_replication_local_address= "10.3.175.61:33061"

# 设置组成员的主机名和端口，新成员使用它们来建立与组的连接
loose-group_replication_group_seeds= "10.3.175.60:33061,10.3.175.61:33061,10.3.175.62:33061"

# 指示插件是否引导组
loose-group_replication_bootstrap_group=off

# 配置单主模式
loose-group_replication_single_primary_mode=ON

# 设置复制组成员白名单
loose-group_replication_ip_whitelist="10.3.172.0/22"

loose-group_replication_enforce_update_everywhere_checks=off

loose-group_replication_allow_local_disjoint_gtids_join=ON

```

- share03

```shell
[root@share03 ~]# vi /etc/my.cnf

# For advice on how to change settings please see
# http://dev.mysql.com/doc/refman/8.0/en/server-configuration-defaults.html

[mysqld]
#
# Remove leading # and set to the amount of RAM for the most important data
# cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
# innodb_buffer_pool_size = 128M
#
# Remove the leading "# " to disable binary logging
# Binary logging captures changes between backups and is enabled by
# default. It's default setting is log_bin=binlog
# disable_log_bin
#
# Remove leading # to set options mainly useful for reporting servers.
# The server defaults are faster for transactions and fast SELECTs.
# Adjust sizes as needed, experiment to find the optimal values.
# join_buffer_size = 128M
# sort_buffer_size = 2M
# read_rnd_buffer_size = 2M
#
# Remove leading # to revert to previous value for default_authentication_plugin,
# this will increase compatibility with older clients. For background, see:
# https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_default_authentication_plugin
# default-authentication-plugin=mysql_native_password

# 数据存放目录
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

# error日志存放目录
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

# 禁用除了innoDB存储引擎之外的存储
disabled_storage_engines=MyISAM,BLACKHOLE,FEDERATED,ARCHIVE,MEMORY

# server id 每台必须拥有不一样的id ，1~2^32 - 1
server_id=62

# 全局事务标识符打开
gtid_mode=ON

enforce_gtid_consistency=ON

#  禁用写入二进制日志的事件的校验和
binlog_checksum=NONE

log_bin=binlog
log_slave_updates=ON

# binlog 格式化为ROW
binlog_format=ROW

master_info_repository=TABLE

relay_log_info_repository=TABLE

# 设置最大连接数
max_connections=200

# 设置最大数据包的大小
max_allowed_packet=16M

# server必须为每个事务收集写集合，并使用XXHASH64哈希算法将其编码为散列
transaction_write_set_extraction=XXHASH64

# 告知插件加入或创建组命名，必须是有效的 UUID
loose-group_replication_group_name="7a774c9d-d328-11eb-a839-fcaa14c7ffe7"

# server启动时不自启组复制,为了避免每次启动自动引导具有相同名称的第二个组,所以设置为OFF
loose-group_replication_start_on_boot=off

# 告诉插件使用IP地址，端口33061用于接收组中其他成员转入连接
loose-group_replication_local_address="10.3.175.62:33061"

# 设置组成员的主机名和端口，新成员使用它们来建立与组的连接
loose-group_replication_group_seeds="10.3.175.60:33061,10.3.175.61:33061,10.3.175.62:33061"

# 指示插件是否引导组
loose-group_replication_bootstrap_group=off

# 配置单主模式
loose-group_replication_single_primary_mode=ON

# 设置复制组成员白名单
loose-group_replication_ip_whitelist="10.3.172.0/22"

loose-group_replication_enforce_update_everywhere_checks=off

loose-group_replication_allow_local_disjoint_gtids_join=ON
```

## 启动 MySQl 实例

```shell
[root@share01 ~]# systemctl start mysqld
[root@share02 ~]# systemctl start mysqld
[root@share03 ~]# systemctl start mysqld
```

## 使用 mysql-shell 连接 MySQL 实例

- share01

```shell
[root@share01 ~]# mysqlsh
MySQL Shell 8.0.25

Copyright (c) 2016, 2021, Oracle and/or its affiliates.
Oracle is a registered trademark of Oracle Corporation and/or its affiliates.
Other names may be trademarks of their respective owners.

Type '\help' or '\?' for help; '\quit' to exit.
MySQL JS > shell.connect('root@10.3.175.60:3306');
Creating a session to 'root@10.3.175.60:3306'
Please provide the password for 'root@10.3.175.60:3306': **\*\***\***\*\***
Save password for 'root@10.3.175.60:3306'? [Y]es/[N]o/Ne[v]er (default No): Y
Fetching schema names for autocompletion... Press ^C to stop.
Your MySQL connection id is 9
Server version: 8.0.25 MySQL Community Server - GPL
No default schema selected; type \use <schema> to set one.
<Session:root@10.3.175.60:3306>
MySQL 10.3.175.60:3306 ssl JS > dba.configureLocalInstance();
Configuring local MySQL instance listening at port 3306 for use in an InnoDB cluster...

This instance reports its own address as share01:3306
Clients and other cluster members will communicate with it through this address by default. If this is not correct, the report_host MySQL system variable should be changed.

applierWorkerThreads will be set to the default value of 4.

NOTE: Some configuration options need to be fixed:
+----------------------------------------+---------------+----------------+----------------------------+
| Variable | Current Value | Required Value | Note |
+----------------------------------------+---------------+----------------+----------------------------+
| binlog_transaction_dependency_tracking | COMMIT_ORDER | WRITESET | Update the server variable |
| slave_parallel_type | DATABASE | LOGICAL_CLOCK | Update the server variable |
| slave_preserve_commit_order | OFF | ON | Update the server variable |
+----------------------------------------+---------------+----------------+----------------------------+

Do you want to perform the required configuration changes? [y/n]: y
Configuring instance...
The instance 'share01:3306' was configured to be used in an InnoDB cluster.
MySQL 10.3.175.60:3306 ssl JS > dba.checkInstanceConfiguration('root@10.3.175.60:3306');
Validating local MySQL instance listening at port 3306 for use in an InnoDB cluster...

This instance reports its own address as share01:3306
Clients and other cluster members will communicate with it through this address by default. If this is not correct, the report_host MySQL system variable should be changed.

Checking whether existing tables comply with Group Replication requirements...
No incompatible tables detected

Checking instance configuration...
Instance configuration is compatible with InnoDB cluster

The instance 'share01:3306' is valid to be used in an InnoDB cluster.

{
"status": "ok"
}
```

- share02

```shell
[root@share02 ~]# mysqlsh
MySQL Shell 8.0.25

Copyright (c) 2016, 2021, Oracle and/or its affiliates.
Oracle is a registered trademark of Oracle Corporation and/or its affiliates.
Other names may be trademarks of their respective owners.

Type '\help' or '\?' for help; '\quit' to exit.
MySQL JS > shell.connect('root@10.3.175.61:3306');
Creating a session to 'root@10.3.175.61:3306'
Please provide the password for 'root@10.3.175.61:3306': **\*\***\***\*\***
Save password for 'root@10.3.175.61:3306'? [Y]es/[N]o/Ne[v]er (default No): Y
Fetching schema names for autocompletion... Press ^C to stop.
Your MySQL connection id is 9
Server version: 8.0.25 MySQL Community Server - GPL
No default schema selected; type \use <schema> to set one.
<Session:root@10.3.175.61:3306>
MySQL 10.3.175.61:3306 ssl JS > dba.configureLocalInstance();
Configuring local MySQL instance listening at port 3306 for use in an InnoDB cluster...

This instance reports its own address as share02:3306
Clients and other cluster members will communicate with it through this address by default. If this is not correct, the report_host MySQL system variable should be changed.

applierWorkerThreads will be set to the default value of 4.

NOTE: Some configuration options need to be fixed:
+----------------------------------------+---------------+----------------+----------------------------+
| Variable | Current Value | Required Value | Note |
+----------------------------------------+---------------+----------------+----------------------------+
| binlog_transaction_dependency_tracking | COMMIT_ORDER | WRITESET | Update the server variable |
| slave_parallel_type | DATABASE | LOGICAL_CLOCK | Update the server variable |
| slave_preserve_commit_order | OFF | ON | Update the server variable |
+----------------------------------------+---------------+----------------+----------------------------+

Do you want to perform the required configuration changes? [y/n]: y
Configuring instance...
The instance 'share02:3306' was configured to be used in an InnoDB cluster.
MySQL 10.3.175.61:3306 ssl JS > dba.checkInstanceConfiguration('root@10.3.175.61:3306');
Validating local MySQL instance listening at port 3306 for use in an InnoDB cluster...

This instance reports its own address as share02:3306
Clients and other cluster members will communicate with it through this address by default. If this is not correct, the report_host MySQL system variable should be changed.

Checking whether existing tables comply with Group Replication requirements...
No incompatible tables detected

Checking instance configuration...
Instance configuration is compatible with InnoDB cluster

The instance 'share02:3306' is valid to be used in an InnoDB cluster.

{
"status": "ok"
}
```

- share03

```shell
[root@share03 ~]# mysqlsh
MySQL Shell 8.0.25

Copyright (c) 2016, 2021, Oracle and/or its affiliates.
Oracle is a registered trademark of Oracle Corporation and/or its affiliates.
Other names may be trademarks of their respective owners.

Type '\help' or '\?' for help; '\quit' to exit.
MySQL JS > shell.connect('root@10.3.175.62:3306');
Creating a session to 'root@10.3.175.62:3306'
Please provide the password for 'root@10.3.175.62:3306': **\*\***\***\*\***
Save password for 'root@10.3.175.62:3306'? [Y]es/[N]o/Ne[v]er (default No): Y
Fetching schema names for autocompletion... Press ^C to stop.
Your MySQL connection id is 9
Server version: 8.0.25 MySQL Community Server - GPL
No default schema selected; type \use <schema> to set one.
<Session:root@10.3.175.62:3306>
MySQL 10.3.175.62:3306 ssl JS > dba.configureLocalInstance();
Configuring local MySQL instance listening at port 3306 for use in an InnoDB cluster...

This instance reports its own address as share03:3306
Clients and other cluster members will communicate with it through this address by default. If this is not correct, the report_host MySQL system variable should be changed.

applierWorkerThreads will be set to the default value of 4.

NOTE: Some configuration options need to be fixed:
+----------------------------------------+---------------+----------------+----------------------------+
| Variable | Current Value | Required Value | Note |
+----------------------------------------+---------------+----------------+----------------------------+
| binlog_transaction_dependency_tracking | COMMIT_ORDER | WRITESET | Update the server variable |
| slave_parallel_type | DATABASE | LOGICAL_CLOCK | Update the server variable |
| slave_preserve_commit_order | OFF | ON | Update the server variable |
+----------------------------------------+---------------+----------------+----------------------------+

Do you want to perform the required configuration changes? [y/n]: y
Configuring instance...
The instance 'share03:3306' was configured to be used in an InnoDB cluster.
MySQL 10.3.175.62:3306 ssl JS > dba.checkInstanceConfiguration('root@10.3.175.62:3306');
Validating local MySQL instance listening at port 3306 for use in an InnoDB cluster...

This instance reports its own address as share03:3306
Clients and other cluster members will communicate with it through this address by default. If this is not correct, the report_host MySQL system variable should be changed.

Checking whether existing tables comply with Group Replication requirements...
No incompatible tables detected

Checking instance configuration...
Instance configuration is compatible with InnoDB cluster

The instance 'share03:3306' is valid to be used in an InnoDB cluster.

{
"status": "ok"
}
```

## 创建 innodb 集群

```shell
MySQL 10.3.175.60:3306 ssl JS > var cluster = dba.createCluster('via_share');
A new InnoDB cluster will be created on instance '10.3.175.60:3306'.

Validating instance configuration at 10.3.175.60:3306...

This instance reports its own address as share01:3306

Instance configuration is suitable.
NOTE: Group Replication will communicate with other members using 'share01:33061'. Use the localAddress option to override.

Creating InnoDB cluster 'via_share' on 'share01:3306'...

Adding Seed Instance...
Cluster successfully created. Use Cluster.addInstance() to add MySQL instances.
At least 3 instances are needed for the cluster to be able to withstand up to
one server failure.
```

## 添加实例节点

- add instance share02

```shell
MySQL 10.3.175.60:3306 ssl JS > cluster.addInstance('root@10.3.175.61:3306');

NOTE: The target instance 'share02:3306' has not been pre-provisioned (GTID set is empty). The Shell is unable to decide whether incremental state recovery can correctly provision it.
The safest and most convenient way to provision a new instance is through automatic clone provisioning, which will completely overwrite the state of 'share02:3306' with a physical snapshot from an existing cluster member. To use this method by default, set the 'recoveryMethod' option to 'clone'.

The incremental state recovery may be safely used if you are sure all updates ever executed in the cluster were done with GTIDs enabled, there are no purged transactions and the new instance contains the same GTID set as the cluster or a subset of it. To use this method by default, set the 'recoveryMethod' option to 'incremental'.

Please select a recovery method [C]lone/[I]ncremental recovery/[A]bort (default Clone): C
Validating instance configuration at 10.3.175.61:3306...

This instance reports its own address as share02:3306

Instance configuration is suitable.
NOTE: Group Replication will communicate with other members using 'share02:33061'. Use the localAddress option to override.

A new instance will be added to the InnoDB cluster. Depending on the amount of
data on the cluster this might take from a few seconds to several hours.

Adding instance to the cluster...

Monitoring recovery process of the new cluster member. Press ^C to stop monitoring and let it continue in background.
Incremental state recovery is now in progress.

- Waiting for distributed recovery to finish...
  NOTE: 'share02:3306' is being recovered from 'share01:3306'
- Distributed recovery has finished

The instance 'share02:3306' was successfully added to the cluster.

```

- add instance share03

```shell
MySQL 10.3.175.60:3306 ssl JS > cluster.addInstance('root@10.3.175.62:3306');

NOTE: The target instance 'share03:3306' has not been pre-provisioned (GTID set is empty). The Shell is unable to decide whether incremental state recovery can correctly provision it.
The safest and most convenient way to provision a new instance is through automatic clone provisioning, which will completely overwrite the state of 'share03:3306' with a physical snapshot from an existing cluster member. To use this method by default, set the 'recoveryMethod' option to 'clone'.

The incremental state recovery may be safely used if you are sure all updates ever executed in the cluster were done with GTIDs enabled, there are no purged transactions and the new instance contains the same GTID set as the cluster or a subset of it. To use this method by default, set the 'recoveryMethod' option to 'incremental'.

Please select a recovery method [C]lone/[I]ncremental recovery/[A]bort (default Clone): C
Validating instance configuration at 10.3.175.62:3306...

This instance reports its own address as share03:3306

Instance configuration is suitable.
NOTE: Group Replication will communicate with other members using 'share03:33061'. Use the localAddress option to override.

A new instance will be added to the InnoDB cluster. Depending on the amount of
data on the cluster this might take from a few seconds to several hours.

Adding instance to the cluster...

Monitoring recovery process of the new cluster member. Press ^C to stop monitoring and let it continue in background.
State recovery already finished for 'share03:3306'

The instance 'share03:3306' was successfully added to the cluster.
```

## 获取集群状态

```shell
MySQL  10.3.175.60:3306 ssl  JS > cluster.status();
{
    "clusterName": "via_share",
    "defaultReplicaSet": {
        "name": "default",
        "primary": "share03:3306",
        "ssl": "REQUIRED",
        "status": "OK",
        "statusText": "Cluster is ONLINE and can tolerate up to ONE failure.",
        "topology": {
            "share01:3306": {
                "address": "share01:3306",
                "memberRole": "SECONDARY",
                "mode": "R/O",
                "readReplicas": {},
                "replicationLag": null,
                "role": "HA",
                "status": "ONLINE",
                "version": "8.0.25"
            },
            "share02:3306": {
                "address": "share02:3306",
                "memberRole": "SECONDARY",
                "mode": "R/O",
                "readReplicas": {},
                "replicationLag": null,
                "role": "HA",
                "status": "ONLINE",
                "version": "8.0.25"
            },
            "share03:3306": {
                "address": "share03:3306",
                "memberRole": "PRIMARY",
                "mode": "R/W",
                "readReplicas": {},
                "replicationLag": null,
                "role": "HA",
                "status": "ONLINE",
                "version": "8.0.25"
            }
        },
        "topologyMode": "Single-Primary"
    },
    "groupInformationSourceMember": "share03:3306"
}

```

## 集群管理

[MySQL InnoDB cluster routine maintenance command](https://dev.mysql.com/doc/mysql-shell/8.0/en/)

## 配置 mysql-router

- share01

```shell
[root@share01 ~]# mysqlrouter --bootstrap root@share01:3306 --user=mysqlrouter
[root@share01 ~]# system start mysqlrouter
[root@share01 ~]# system status mysqlrouter
[root@share01 ~]# system enable mysqld
[root@share01 ~]# system enable mysqlrouter
```

- share02

```shell
[root@share02 ~]# mysqlrouter --bootstrap root@share02:3306 --user=mysqlrouter
[root@share02 ~]# system start mysqlrouter
[root@share02 ~]# system status mysqlrouter
[root@share02 ~]# system enable mysqld
[root@share02 ~]# system enable mysqlrouter
```

- share03

```shell
[root@share03 ~]# mysqlrouter --bootstrap root@share03:3306 --user=mysqlrouter
[root@share03 ~]# system start mysqlrouter
[root@share03 ~]# system status mysqlrouter
[root@share03 ~]# system enable mysqld
[root@share03 ~]# system enable mysqlrouter
```
