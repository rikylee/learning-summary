# cert-manager Let's Encrypt 实现 Nginx ingress TLS 自动证书签发

cert-manager 是一个云原生证书管理开源项目，用于在 Kubernetes 集群中提供 HTTPS 证书并自动续期，支持 Let’s Encrypt, HashiCorp Vault 和 Venafi 这些免费证书的签发。在 Kubernetes 集群中，我们可以通过 Kubernetes Ingress 和 Let’s Encrypt 实现外部服务的自动化 HTTPS，详细请参看下图

![](https://gitee.com/rikylee/images/raw/master/image/202112091010224.png)

## cert-manager 组件

- Issuer：Issuer（只能在单个命名空间使用）、 和 ClusterIssuer（多个命名空间共用） 是代表证书颁发机构 (CA) 的 Kubernetes 资源，这些证书颁发机构能够通过接受证书签名请求来生成签名证书。所有 cert-manager 证书都需要一个处于就绪状态的引用颁发者来尝试接受请求。
- Certificate：定义所需的 X.509 证书，该证书将更新并保持最新，Certificate 只能在单个命名空间使用，会引用 Issuer\ClusterIssuer

- CertificateRequest: 用于从 Issuer 请求 X.509 证书

- ACME Orders and Challenges：支持使用 ACME Issuer 从 ACME 服务器（包括 Let's Encrypt ）请求证书，并管理证书的生命周期

- Webhook：cert-manager 通过使用 Webhook 服务器扩展 Kubernetes API 服务器来提供 对 cert-manager 资源的动态准入控制
- CA Injector：帮助配置 CA 证书

## 安装 cert-manager,参看[cert-manager 官方文档](https://cert-manager.io/docs/installation/)

仅涉及到 cert-manager 相关操作，k8s 和 ingress 相关基础此处默认都会

- 通过 yaml 安装

```shell
[root@k8s-master ~]# kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
```

涉及到如下 3 个镜像:

```shell
docker pull quay.io/jetstack/cert-manager-cainjector:v1.6.1
docker pull quay.io/jetstack/cert-manager-controller:v1.6.1
docker pull quay.io/jetstack/cert-manager-webhook:v1.6.1
```

查看安装结果

```shell
[root@k8s-master ~]# kubectl get pods -n cert-manager
NAME                                      READY   STATUS    RESTARTS   AGE
cert-manager-86dfd6965c-drdf7             1/1     Running   0          98d
cert-manager-cainjector-65cd7dd78-v7ppn   1/1     Running   0          98d
cert-manager-webhook-558c594f5f-49fn7     1/1     Running   0          98d

```

- 通过 helm chart 安装，需要 helm 3.0+

需要添加 jetstack 的 Helm 库

```shell
[root@k8s-master ~]# helm repo add jetstack https://charts.jetstack.io
```

更新本地 helm 库缓存

```shell
[root@k8s-master ~]# helm repo update
```

安装

```shell
[root@k8s-master ~]# helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.6.1 \
  --set installCRDs=true
```

## 使用 cert-manager

- 先创建一个 Issuer,,指定使用 letsencrypt

```shell

[root@k8s-master ~]# kubectl create ns cert-test

[root@k8s-master ~]# cat <<EOF > cert-test-issuer.yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-prod
  namespace: cert-test
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-prod
    solvers:
      - http01:
          ingress:
            class: nginx
EOF

[root@k8s-master ~]# kubectl apply -f cert-test-issuer.yaml

[root@k8s-master ~]# kubectl get Issuer -n cert-test
NAME               READY   AGE
letsencrypt-prod   True    20d

```

- 创建 Certificate,涉及到的域名如：test.domain.com,请先做好域名解析，确保能指向 K8S 集群 work node

```shell
[root@k8s-master ~]# cat <<EOF > cert-test-certificate.yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  labels:
    app: cert-test-certificate
  name: cert-test-certificate
  namespace: cert-test
spec:
  dnsNames:
  - test.domain.com
  issuerRef:
    group: cert-manager.io
    kind: Issuer
    name: letsencrypt-prod
  secretName: cert-test-tls
EOF

[root@k8s-master ~]# kubectl apply -f cert-test-certificate.yaml

[root@k8s-master ~]# kubectl get Certificate -n cert-test
NAME                    READY   SECRET           AGE
cert-test-certificate   True    cert-test-tls    20d

```

- 创建 ingress,提前部署好测试的服务

```shell

[root@k8s-master ~]# cat <<EOF > cert-test-ingress.yaml
kind: Ingress
apiVersion: extensions/v1beta1
metadata:
  name: cert-test-ingress
  namespace: cert-test
  labels:
    app: cert-test-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/issuer: 'letsencrypt-prod'
spec:
  tls:
  - hosts:
    - test.domain.com
    secretName: cert-test-tls
  rules:
    - host: test.domain.com
      http:
        paths:
          - path: /
            backend:
              serviceName: nginx
              servicePort: 80
EOF

[root@k8s-master ~]# kubectl apply -f cert-test-ingress.yaml
[root@k8s-master ~]# kubectl get ingress -n cert-test
NAME                HOSTS             ADDRESS       PORTS     AGE
cert-test-ingress   test.domain.com   10.3.180.1,   80, 443   20d

```

- 浏览器访问域名相关服务，查看证书状态

![查看证书](https://gitee.com/rikylee/images/raw/master/image/202112091106383.png)
