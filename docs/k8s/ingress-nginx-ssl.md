# 记一次ingress nginx 使用 Godaddy SSL证书 异常

- 下载适用于nginx的Apache证书，解压之后文件如下

  ``` shell
  $ ll
  total 20
  -rw-r--r-- 1 Riky Li 197121 2248 一月  8 02:44 3f8368756f7e8925.crt
  -rw-r--r-- 1 Riky Li 197121 2248 一月  8 02:44 3f8368756f7e8925.pem
  -rw-r--r-- 1 Riky Li 197121 4795 一月  8 02:44 gd_bundle-g2-g1.crt
  -rw-r--r-- 1 Riky Li 197121 1704 一月 12 13:19 generated-private-key.txt

  ```

  我们需要用到的3f8368756f7e8925.crt、gd_bundle-g2-g1.crt、generated-private-key.txt这三个文件

- 处理文件

  ```shell
  [root@k8s-portal-master1 tls]# cp generated-private-key.txt tls.key
  [root@k8s-portal-master1 tls]# cat 3f8368756f7e8925.crt gd_bundle-g2-g1.crt > tls.crt
  ```

- 创建TLS密钥

  ```shell
  [root@k8s-portal-master1 tls]# kubectl -n portal create secret tls domain-tls --cert ./tls.crt --key ./tls.key
  error: failed to load key pair tls: failed to find any PEM data in key input
  ```

  出现`failed to load key pair tls: failed to find any PEM data in key input`错误，使用Notepad++打开原generated-private-key.txt文件，发现文件的编码格式为UTF-8-BOM，切换文件编码为UTF-8，重新保存

  ```shell
  [root@k8s-portal-master1 tls]# cp generated-private-key.txt tls.key
  cp: overwrite ‘tls.key’? y
  [root@k8s-portal-master1 tls]# kubectl -n olami-portal create secret tls domain-tls --cert ./tls.crt --key ./tls.key
  secret/domain-tls created

  ```

  密钥创建成功

- 更新需要使用证书的ingress,启用TLS认证

  ```
  kind: Ingress
  apiVersion: extensions/v1beta1
  metadata:
    name: ui-ingress
    namespace: portal
    annotations:
      kubernetes.io/ingress.class: nginx
      nginx.ingress.kubernetes.io/ssl-redirect: 'true'
  spec:
    tls:
      - hosts:
          - www.domain.com
        secretName: domain-tls
    rules:
      - host: www.domain.com
        http:
          paths:
            - path: /
              backend:
                serviceName: ui
                servicePort: 80

  ```

  访问`https://www.domain.com`，页面打开之后显示证书错误，查看之后并非预期的Godaddy的证书，还是ingress-nginx默认的fake的证书

- 重启daemonset ingress-nginx-controller 所有的Pod

  ingress新增的TLS的配置之后未生效，只好重启所有的ingress-nginx-controller pod,以便完成ingress的更新，所有Pod重启完成后访问`https://www.domain.com`,页面正常显示，证书加载正常
