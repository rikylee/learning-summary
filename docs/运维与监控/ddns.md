# 基于阿里云解析实现DDNS内网服务穿透

- 创建工作目录和docker-compose.yaml文件

```bash
#!/bin/bash
ROOT_DIR=$(cd `dirname $0` && pwd)
WORK_DIR="/data/work"
makdir -p $WORK_DIR

sudo tee $WORK_DIR/docker-compose.yaml <<-'EOF'
version: "3"
services:
  ddns:
    image: newfuture/ddns:v2.12.0"
    hostname: "ddns-server"
    restart: always
    container_name: "ddns"
    labels:
      - app-group=ddns
    volumes:
      - /data/work/ddns_config.json:/config.json
    network_mode: host
  nginx:
    image: "nginx:1.23.4-alpine"
    hostname: "nginx"
    container_name: "nginx"
    restart: always
    labels:
      - app-group=nginx
    ports:
      - "80:80/tcp"
EOF
```

- 生成对应的ddns配置，然后启动服务

```bash
#!/bin/bash
# 获取当前目录
ROOT_DIR=$(cd `dirname $0` && pwd)
WORK_DIR="/data/work"

# 网卡名称
INTERFACE_NAME="eth0"
# 获取本机的IP
# HOST_IP=$(ifconfig $INTERFACE_NAME | grep inet | grep -v inet6 | awk '{ print $2}')
# 获取本机的mac
MAC=$(ifconfig $INTERFACE_NAME |grep ether | awk '{ print $2}')
# 去除mac中:字符
PIN=$(echo $MAC | sed -e 's|:||g' | tr [A-F] [a-f])

# 域名
DOMAIN=ddns.demo.com

# 填入阿里云解析的AAPKEY和SECRET
ALI_APPKEY=""
ALI_SECRET=""

sudo tee $WORK_DIR/ddns_config.json <<-'EOF'
{
  "$schema": "https://ddns.newfuture.cc/schema/v2.8.json",
  "id": "12345",
  "token": "mytokenkey",
  "dns": "alidns",
  "ipv4": ["ddns.domain.com"],
  "ipv6": [],
  "index4": "public",
  "index6": 0,
  "ttl": 600,
  "debug": false,
  "cache": true
}
EOF

# 替换阿里云APPKEY和SECRET
sudo sed -i "s/12345/${ALI_APPKEY}/g" $WORK_DIR/ddns_config.json
sudo sed -i "s/mytokenkey/${ALI_SECRET}/g" $WORK_DIR/ddns_config.json
# 使用mac地址前六位拼接二级域名，作为三级域名
sudo sed -i "s/ddns.domain.com/${PIN:6}.${DOMAIN}/g" $WORK_DIR/ddns_config.json

cd $WORK_DIR

# 启动服务
docker compose up -d
```

- 路由器上配置端口转发，如使用10000端口转发到服务器192.168.1.3:80端口

- 使用域名访问内部服务例如:`http://48b02d.ddns.demo.com:10000`,三级域名为服务器MAC地址的前六位字符串
