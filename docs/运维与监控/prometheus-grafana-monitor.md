# 基于 prometheus+grafana 的监控

## install prometheus server on share01

```shell
admin@share01:~$ mkdir -p prometheus &&  cd prometheus
admin@share01:~/prometheus$ sudo mkdir -p /etc/prometheus
admin@share01:~/prometheus$ wget https://github.com/prometheus/prometheus/releases/download/v2.29.0-rc.1/prometheus-2.29.0-rc.1.linux-amd64.tar.gz
admin@share01:~/prometheus$ tar xf prometheus-2.29.0-rc.1.linux-amd64.tar.gz
admin@share01:~/prometheus$ sudo cp prometheus-2.29.0-rc.1.linux-amd64/prom* /usr/local/bin/
admin@share01:~/prometheus$ sudo cp prometheus-2.29.0-rc.1.linux-amd64/prometheus.yml /etc/prometheus/
admin@share01:~/prometheus$ sudo vi /etc/systemd/system/prometheus.service
[Unit]
Description=Prometheus Server
After=network.target

[Service]
ExecStart=/usr/local/bin/prometheus --config.file="/etc/prometheus/prometheus.yml" --web.listen-address="0.0.0.0:9090" --storage.tsdb.path=/var/lib/prometheus/ --web.enable-lifecycle

[Install]
WantedBy=multi-user.target

admin@share01:~/prometheus$ sudo systemctl daemon-reload
admin@share01:~/prometheus$ sudo systemctl enable --now prometheus
admin@share01:~/prometheus$ sudo systemctl status prometheus
admin@share01:~/prometheus$ curl 127.0.0.1:9090
<a href="/graph">Found</a>.

```

## install alertmanager on share01

- install alertmanager

```shell
admin@share01:~/prometheus$ wget https://github.com/prometheus/alertmanager/releases/download/v0.22.2/alertmanager-0.22.2.linux-amd64.tar.gz
admin@share01:~/prometheus$ tar xf alertmanager-0.22.2.linux-amd64.tar.gz
admin@share01:~/prometheus$ sudo mkdir -p /etc/alertmanager
admin@share01:~/prometheus$ sudo cp alertmanager-0.22.2.linux-amd64/alertmanager.yml /etc/alertmanager/
admin@share01:~/prometheus$ sudo cp alertmanager-0.22.2.linux-amd64/alertmanager /usr/local/bin/
admin@share01:~/prometheus$ sudo cp alertmanager-0.22.2.linux-amd64/amtool /usr/local/bin/
admin@share01:~/prometheus$ sudo vi /etc/systemd/system/alertmanager.service
[Unit]
Description=Alertmanager Server
After=network.target

[Service]
ExecStart=/usr/local/bin/alertmanager --config.file="/etc/alertmanager/alertmanager.yml" --web.listen-address="0.0.0.0:9093" --storage.path=/var/lib/alertmanager/

[Install]
WantedBy=multi-user.target


admin@share01:~/prometheus$ sudo systemctl daemon-reload
admin@share01:~/prometheus$ sudo systemctl enable --now alertmanager
admin@share01:~/prometheus$ sudo systemctl status alertmanager

```

- config alertmanager

```shell
admin@share01:~/prometheus$ sudo vi /etc/alertmanager/alertmanager.yml
global:
  resolve_timeout: 30m
  smtp_smarthost: 'mail.via.com.tw:587' #smtp host:port
  smtp_from: 'no-reply@viaheadway.com'
  smtp_auth_username: 'china\\no-reply'
  smtp_auth_password: 'xxxxxxx'
  smtp_require_tls: true

route:
  group_by: ['alertname']
  group_wait: 30s
  group_interval: 60s
  repeat_interval: 4h
  receiver: 'email-receiver'
receivers:
  - name: 'email-receiver'
    email_configs:
    - to: 'rikyli@viatech.com.cn'
      send_resolved: true
inhibit_rules:
  - source_match:
      everity: 'critical'
    target_match:
      severity: 'warning'
    equal: ['alertname','instance']
```

## install node_exporter on share01,via-share02,via-share03

```shell
admin@share01:~/prometheus$ wget https://github.com/prometheus/node_exporter/releases/download/v1.2.0/node_exporter-1.2.0.linux-amd64.tar.gz
admin@share01:~/prometheus$ tar xf node_exporter-1.2.0.linux-amd64.tar.gz
admin@share01:~/prometheus$ sudo cp node_exporter-1.2.0.linux-amd64/node_exporter /usr/sbin/
admin@share01:~/prometheus$ sudo useradd -M -s /usr/sbin/nologin  node_exporter

admin@share01:~/prometheus$ sudo vi /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
Documentation=node_exporter Monitoring System
After=network.target

[Service]
User=node_exporter
ExecStart=/usr/sbin/node_exporter --web.listen-address=:9100

[Install]
WantedBy=multi-user.target

admin@share01:~/prometheus$ sudo systemctl daemon-reload
admin@share01:~/prometheus$ sudo systemctl enable --now node_exporter
admin@share01:~/prometheus$ sudo systemctl status node_exporter
admin@share01:~/prometheus$ sudo apt  install curl

admin@share01:~/prometheus$ curl 127.0.0.1:9100
<html>
  <head><title>Node Exporter</title></head>
  <body>
  <h1>Node Exporter</h1>
  <p><a href="/metrics">Metrics</a></p>
  </body>
</html>
```

## install mysqld-exporter

- create monitor user

```shell
admin@share01:~/prometheus$ mysql -h 192.168.1.161 -P 6446 -uroot -ppwd

mysql> CREATE USER 'exporter'@'%' IDENTIFIED BY 'pwd@exporter' WITH MAX_USER_CONNECTIONS 9;
Query OK, 0 rows affected (0.02 sec)

mysql> GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'%';
Query OK, 0 rows affected (0.01 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.01 sec)

mysql> exit
Bye
```

- install mysqld-exporter on share01,via-share02,via-share03

```shell
admin@share01:~/prometheus$ wget https://github.com/prometheus/mysqld_exporter/releases/download/v0.13.0/mysqld_exporter-0.13.0.linux-amd64.tar.gz


admin@share01:~/prometheus$ tar xf mysqld_exporter-0.13.0.linux-amd64.tar.gz

admin@share01:~/prometheus$ sudo mkdir -p /etc/mysqld_exporter

admin@share01:~/prometheus$ sudo vi /etc/mysqld_exporter/.my.cnf
[client]
host=127.0.0.1
port=3306
user=exporter
password=pwd@exporter

admin@share01:~/prometheus$ sudo cp ./mysqld_exporter-0.13.0.linux-amd64/mysqld_exporter /usr/local/bin/

admin@share01:~/prometheus$ sudo vi /etc/systemd/system/mysqld_exporter.service
[Unit]
Description=mysqld_exporter
After=network.target
[Service]
Type=simple
ExecStart=/usr/local/bin/mysqld_exporter --web.listen-address=0.0.0.0:9104 \
  --config.my-cnf=/etc/mysqld_exporter/.my.cnf \
  --collect.slave_status \
  --collect.slave_hosts \
  --log.level=error \
  --collect.info_schema.processlist \
  --collect.info_schema.innodb_metrics \
  --collect.info_schema.innodb_tablespaces \
  --collect.info_schema.innodb_cmp \
  --collect.info_schema.innodb_cmpmem
Restart=on-failure
[Install]
WantedBy=multi-user.target


admin@share01:~/prometheus$ sudo systemctl daemon-reload
admin@share01:~/prometheus$ sudo systemctl enable --now mysqld_exporter
admin@share01:~/prometheus$ sudo systemctl status mysqld_exporter

```

## install Blackbox exporter if you need it

The [blackbox exporter](https://github.com/prometheus/blackbox_exporter) allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP and ICMP.

## create prometheus rules

- node Recording rules

```shell
admin@share01:~/prometheus$ sudo mkdir -p /etc/prometheus/rules
admin@share01:~/prometheus$ sudo vi /etc/prometheus/rules/node-exporter-record.yml
"groups":
- "name": "node-exporter.rules"
  "rules":
  - "expr": |
      count without (cpu) (
        count without (mode) (
          node_cpu_seconds_total{job="node"}
        )
      )
    "record": "instance:node_num_cpu:sum"
  - "expr": |
      1 - avg without (cpu, mode) (
        rate(node_cpu_seconds_total{job="node", mode="idle"}[1m])
      )
    "record": "instance:node_cpu_utilisation:rate1m"
  - "expr": |
      (
        node_load1{job="node"}
      /
        instance:node_num_cpu:sum{job="node"}
      )
    "record": "instance:node_load1_per_cpu:ratio"
  - "expr": |
      1 - (
        node_memory_MemAvailable_bytes{job="node"}
      /
        node_memory_MemTotal_bytes{job="node"}
      )
    "record": "instance:node_memory_utilisation:ratio"
  - "expr": |
      rate(node_vmstat_pgmajfault{job="node"}[1m])
    "record": "instance:node_vmstat_pgmajfault:rate1m"
  - "expr": |
      rate(node_disk_io_time_seconds_total{job="node", device!=""}[1m])
    "record": "instance_device:node_disk_io_time_seconds:rate1m"
  - "expr": |
      rate(node_disk_io_time_weighted_seconds_total{job="node", device!=""}[1m])
    "record": "instance_device:node_disk_io_time_weighted_seconds:rate1m"
  - "expr": |
      sum without (device) (
        rate(node_network_receive_bytes_total{job="node", device!="lo"}[1m])
      )
    "record": "instance:node_network_receive_bytes_excluding_lo:rate1m"
  - "expr": |
      sum without (device) (
        rate(node_network_transmit_bytes_total{job="node", device!="lo"}[1m])
      )
    "record": "instance:node_network_transmit_bytes_excluding_lo:rate1m"
  - "expr": |
      sum without (device) (
        rate(node_network_receive_drop_total{job="node", device!="lo"}[1m])
      )
    "record": "instance:node_network_receive_drop_excluding_lo:rate1m"
  - "expr": |
      sum without (device) (
        rate(node_network_transmit_drop_total{job="node", device!="lo"}[1m])
      )
    "record": "instance:node_network_transmit_drop_excluding_lo:rate1m"
```

- node Alerting rules

```shell
admin@share01:~/prometheus$ sudo vi /etc/prometheus/rules/node-exporter-alert.yml
"groups":
- "name": "node-exporter"
  "rules":
  - "alert": "NodeDown"
    expr: count by (job,instance) (up{job="node"}==0)>0
    for: 60s
    labels:
      severity: critical
    annotations:
      message: 'node {{ $labels.instance }} has been down more than 60s.'
  - alert: hostCpuUsageAlert
    expr: 100 - (avg by(instance,job) (irate(node_cpu_seconds_total{job="node",mode="idle"}[5m])) * 100) > 60
    for: 5m
    labels:
      severity: warning
    annotations:
      message: "node {{ $labels.instance }} CPU usage high ,current: {{ $value }}%"
  - alert: hostMemUsageAlert
    expr: (node_memory_MemTotal_bytes{job="node"} - node_memory_MemAvailable_bytes{job="node"}) / node_memory_MemTotal_bytes{job="node"} * 100 > 70
    for: 5m
    labels:
      severity: warning
    annotations:
      message: "node {{ $labels.instance }} MEM usage high ,current: {{ $value }}%"
  - "alert": "NodeFilesystemSpaceFillingUp"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available space left and is filling up."
      "summary": "Filesystem is predicted to run out of space within the next 24 hours."
    "expr": |
      (
        node_filesystem_avail_bytes{job="node",fstype!=""} / node_filesystem_size_bytes{job="node",fstype!=""} * 100 < 40
      and
        predict_linear(node_filesystem_avail_bytes{job="node",fstype!=""}[6h], 24*60*60) < 0
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "warning"
  - "alert": "NodeFilesystemSpaceFillingUp"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available space left and is filling up fast."
      "summary": "Filesystem is predicted to run out of space within the next 4 hours."
    "expr": |
      (
        node_filesystem_avail_bytes{job="node",fstype!=""} / node_filesystem_size_bytes{job="node",fstype!=""} * 100 < 20
      and
        predict_linear(node_filesystem_avail_bytes{job="node",fstype!=""}[6h], 4*60*60) < 0
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "critical"
  - "alert": "NodeFilesystemAlmostOutOfSpace"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available space left."
      "summary": "Filesystem has less than 5% space left."
    "expr": |
      (
        node_filesystem_avail_bytes{job="node",fstype!=""} / node_filesystem_size_bytes{job="node",fstype!=""} * 100 < 5
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "warning"
  - "alert": "NodeFilesystemAlmostOutOfSpace"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available space left."
      "summary": "Filesystem has less than 3% space left."
    "expr": |
      (
        node_filesystem_avail_bytes{job="node",fstype!=""} / node_filesystem_size_bytes{job="node",fstype!=""} * 100 < 3
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "critical"
  - "alert": "NodeFilesystemFilesFillingUp"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available inodes left and is filling up."
      "summary": "Filesystem is predicted to run out of inodes within the next 24 hours."
    "expr": |
      (
        node_filesystem_files_free{job="node",fstype!=""} / node_filesystem_files{job="node",fstype!=""} * 100 < 40
      and
        predict_linear(node_filesystem_files_free{job="node",fstype!=""}[6h], 24*60*60) < 0
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "warning"
  - "alert": "NodeFilesystemFilesFillingUp"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available inodes left and is filling up fast."
      "summary": "Filesystem is predicted to run out of inodes within the next 4 hours."
    "expr": |
      (
        node_filesystem_files_free{job="node",fstype!=""} / node_filesystem_files{job="node",fstype!=""} * 100 < 20
      and
        predict_linear(node_filesystem_files_free{job="node",fstype!=""}[6h], 4*60*60) < 0
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "critical"
  - "alert": "NodeFilesystemAlmostOutOfFiles"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available inodes left."
      "summary": "Filesystem has less than 5% inodes left."
    "expr": |
      (
        node_filesystem_files_free{job="node",fstype!=""} / node_filesystem_files{job="node",fstype!=""} * 100 < 5
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "warning"
  - "alert": "NodeFilesystemAlmostOutOfFiles"
    "annotations":
      "description": "Filesystem on {{ $labels.device }} at {{ $labels.instance }} has only {{ printf \"%.2f\" $value }}% available inodes left."
      "summary": "Filesystem has less than 3% inodes left."
    "expr": |
      (
        node_filesystem_files_free{job="node",fstype!=""} / node_filesystem_files{job="node",fstype!=""} * 100 < 3
      and
        node_filesystem_readonly{job="node",fstype!=""} == 0
      )
    "for": "1h"
    "labels":
      "severity": "critical"
  - "alert": "NodeNetworkReceiveErrs"
    "annotations":
      "description": "{{ $labels.instance }} interface {{ $labels.device }} has encountered {{ printf \"%.0f\" $value }} receive errors in the last two minutes."
      "summary": "Network interface is reporting many receive errors."
    "expr": |
      rate(node_network_receive_errs_total[2m]) / rate(node_network_receive_packets_total[2m]) > 0.01
    "for": "1h"
    "labels":
      "severity": "warning"
  - "alert": "NodeNetworkTransmitErrs"
    "annotations":
      "description": "{{ $labels.instance }} interface {{ $labels.device }} has encountered {{ printf \"%.0f\" $value }} transmit errors in the last two minutes."
      "summary": "Network interface is reporting many transmit errors."
    "expr": |
      rate(node_network_transmit_errs_total[2m]) / rate(node_network_transmit_packets_total[2m]) > 0.01
    "for": "1h"
    "labels":
      "severity": "warning"
  - "alert": "NodeHighNumberConntrackEntriesUsed"
    "annotations":
      "description": "{{ $value | humanizePercentage }} of conntrack entries are used."
      "summary": "Number of conntrack are getting close to the limit."
    "expr": |
      (node_nf_conntrack_entries / node_nf_conntrack_entries_limit) > 0.75
    "labels":
      "severity": "warning"
  - "alert": "NodeTextFileCollectorScrapeError"
    "annotations":
      "description": "Node Exporter text file collector failed to scrape."
      "summary": "Node Exporter text file collector failed to scrape."
    "expr": |
      node_textfile_scrape_error{job="node"} == 1
    "labels":
      "severity": "warning"
  - "alert": "NodeClockSkewDetected"
    "annotations":
      "description": "Clock on {{ $labels.instance }} is out of sync by more than 300s. Ensure NTP is configured correctly on this host."
      "summary": "Clock skew detected."
    "expr": |
      (
        node_timex_offset_seconds > 0.05
      and
        deriv(node_timex_offset_seconds[5m]) >= 0
      )
      or
      (
        node_timex_offset_seconds < -0.05
      and
        deriv(node_timex_offset_seconds[5m]) <= 0
      )
    "for": "10m"
    "labels":
      "severity": "warning"
  - "alert": "NodeClockNotSynchronising"
    "annotations":
      "description": "Clock on {{ $labels.instance }} is not synchronising. Ensure NTP is configured on this host."
      "summary": "Clock not synchronising."
    "expr": |
      min_over_time(node_timex_sync_status[5m]) == 0
      and
      node_timex_maxerror_seconds >= 16
    "for": "10m"
    "labels":
      "severity": "warning"
  - "alert": "NodeRAIDDegraded"
    "annotations":
      "description": "RAID array '{{ $labels.device }}' on {{ $labels.instance }} is in degraded state due to one or more disks failures. Number of spare drives is insufficient to fix issue automatically."
      "summary": "RAID Array is degraded"
    "expr": |
      node_md_disks_required - ignoring (state) (node_md_disks{state="active"}) > 0
    "for": "15m"
    "labels":
      "severity": "critical"
  - "alert": "NodeRAIDDiskFailure"
    "annotations":
      "description": "At least one device in RAID array on {{ $labels.instance }} failed. Array '{{ $labels.device }}' needs attention and possibly a disk swap."
      "summary": "Failed device in RAID array"
    "expr": |
      node_md_disks{state="failed"} > 0
    "labels":
      "severity": "warning"
```

- mysql Recording rules

```shell
admin@share01:~/prometheus$ sudo vi /etc/prometheus/rules/mysql-record.yml
groups:
- name: mysqld_rules
  rules:

  # Record slave lag seconds for pre-computed timeseries that takes
  # `mysql_slave_status_sql_delay` into account
  - record: instance:mysql_slave_lag_seconds
    expr: mysql_slave_status_seconds_behind_master - mysql_slave_status_sql_delay

  # Record slave lag via heartbeat method
  - record: instance:mysql_heartbeat_lag_seconds
    expr: mysql_heartbeat_now_timestamp_seconds - mysql_heartbeat_stored_timestamp_seconds

  - record: job:mysql_transactions:rate5m
    expr: sum without (command) (rate(mysql_global_status_commands_total{command=~"(commit|rollback)"}[5m]))

```

- mysql Alerting rules

```shell
admin@share01:~/prometheus$ sudo vi /etc/prometheus/rules/mysql-alert.yml
groups:
- name: MySQLdAlerts
  rules:
  - alert: MySQLDown
    expr: mysql_up != 1
    for: 2m
    labels:
      severity: critical
    annotations:
      description: 'MySQL {{$labels.job}} on {{$labels.instance}} is not up.'
      summary: MySQL not up
      message: 'MySQL {{$labels.job}} on {{$labels.instance}} is not up.'
  - alert: MysqlOpenFilesHigh
    expr: mysql_global_status_innodb_num_open_files >(mysql_global_variables_open_files_limit) * 0.75
    for: 60s
    labels:
      severity: warning
    annotations:
      message: 'MySQL {{ $labels.job }} on {{ $labels.pod }} the number of open files has exceeded 75% of the maximum limit.'
  - alert: MysqlTooManyConnections
    expr: rate(mysql_global_status_threads_connected[5m])>200
    for: 5m
    labels:
      severity: warning
    annotations:
      message: 'MySQL {{ $labels.job }} on {{ $labels.pod }} more than 200 connections in 5 minutes'
  - alert: MysqlTooManySlowQueries
    expr: rate(mysql_global_status_slow_queries[5m])>3
    for: 5m
    labels:
      severity: warning
    annotations:
      message: 'MySQL {{ $labels.job }} on {{ $labels.pod }} slow query for more than 3 times in 5 minutes'
  - alert: MySQLReplicationNotRunning
    expr: mysql_slave_status_slave_io_running == 0 or mysql_slave_status_slave_sql_running
      == 0
    for: 2m
    labels:
      severity: critical
    annotations:
      description: Slave replication (IO or SQL) has been down for more than 2 minutes.
      summary: Slave replication is not running
  - alert: MySQLReplicationLag
    expr: (instance:mysql_slave_lag_seconds > 30) and on(instance) (predict_linear(instance:mysql_slave_lag_seconds[5m],
      60 * 2) > 0)
    for: 1m
    labels:
      severity: critical
    annotations:
      description: The mysql slave replication has fallen behind and is not recovering
      summary: MySQL slave replication is lagging
  - alert: MySQLReplicationHeartBeatLag
    expr: (instance:mysql_heartbeat_lag_seconds > 30) and on(instance) (predict_linear(instance:mysql_heartbeat_lag_seconds[5m],
      60 * 2) > 0)
    for: 1m
    labels:
      severity: critical
    annotations:
      description: The mysql slave replication has fallen behind and is not recovering
      summary: MySQL slave replication is lagging
  - alert: MySQLInnoDBLogWaits
    expr: rate(mysql_global_status_innodb_log_waits[15m]) > 10
    labels:
      severity: warning
    annotations:
      description: The innodb logs are waiting for disk at a rate of {{$value}} /
        second
      summary: MySQL innodb log writes stalling
```

- [minio Alerting rules](https://github.com/minio/minio/blob/master/docs/metrics/prometheus/list.md)

## config prometheus server

- generate minio prometheus bearer_token to access `/minio/v2/metrics/cluster`

```shell
admin@share01:~/minio-work$ ./mc admin prometheus generate vsminio  ## mc admin prometheus generate <alias>
scrape_configs:
- job_name: minio-job
  bearer_token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjQ3ODIyNDY3NzksImlzcyI6InByb21ldGhldXMiLCJzdWIiOiJhZG1pbiJ9.Gz3L-lrwxU4GAU4s53Uk2TAsw1LANZJuZS_jng6H6yTgclWzO-JISKBNfbUdaUwSlFF_PK0qRIr9PpQ0ES1-Kg
  metrics_path: /minio/v2/metrics/cluster
  scheme: http
  static_configs:
  - targets: ['localhost:9000']

```

- config alerting,scrape_configs

```shell
admin@share01:~/prometheus$ sudo vi /etc/prometheus/prometheus.yml
# my global config
global:
  scrape_interval: 15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
    - static_configs:
        - targets:
           - 127.0.0.1:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  - "/etc/prometheus/rules/*.yml"
  # - "first_rules.yml"
  # - "second_rules.yml"
# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: "prometheus"

    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.

    static_configs:
      - targets: ["localhost:9090"]
  - job_name: "host"  # monitor host
    static_configs:
      - targets:
          - 192.168.1.161:9100
          - 192.168.1.162:9100
          - 192.168.1.163:9100
  - job_name: "mysql" # monitor mysql
    static_configs:
      - targets:
          - 192.168.1.161:9104
          - 192.168.1.162:9104
          - 192.168.1.163:9104
  - job_name: "minio-job" # monitor minio
    bearer_token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjQ3ODIyNDY3NzksImlzcyI6InByb21ldGhldXMiLCJzdWIiOiJhZG1pbiJ9.Gz3L-lrwxU4GAU4s53Uk2TAsw1LANZJuZS_jng6H6yTgclWzO-JISKBNfbUdaUwSlFF_PK0qRIr9PpQ0ES1-Kg
    metrics_path: /minio/v2/metrics/cluster
    scheme: http
    static_configs:
      - targets:
        - 192.168.1.161:9000
        - 192.168.1.162:9000
        - 192.168.1.163:9000
```

## install grafana on share01

```shell
admin@share01:~/prometheus$ sudo apt-get install -y apt-transport-https
admin@share01:~/prometheus$ sudo apt-get install -y software-properties-common wget
admin@share01:~/prometheus$ wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -

admin@share01:~/prometheus$ echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

admin@share01:~/prometheus$ sudo apt-get update
admin@share01:~/prometheus$ sudo apt-get install grafana

admin@share01:~/prometheus$ sudo systemctl daemon-reload
admin@share01:~/prometheus$ sudo systemctl start grafana-server
admin@share01:~/prometheus$ sudo systemctl status grafana-server
```

## install grafana alertmanager plugin

```shell
admin@share01:~/prometheus$ sudo grafana-cli plugins install camptocamp-prometheus-alertmanager-datasource
[sudo] password for admin:
installing camptocamp-prometheus-alertmanager-datasource @ 1.0.0
from: https://grafana.com/api/plugins/camptocamp-prometheus-alertmanager-datasource/versions/1.0.0/download
into: /var/lib/grafana/plugins

✔ Installed camptocamp-prometheus-alertmanager-datasource successfully

Restart grafana after installing plugins . <service grafana-server restart>

admin@share01:~/prometheus$ sudo systemctl restart grafana-server

```
