# 基于 named DNS 服务器搭建与配置

## DNS 查询方式

- 迭代查询
  客户端向本地 DNS 服务器发出请求后，一直处于等待状态，直到本地名称服务器返回查询结果。以访问 www.baidu.com 为例叙述迭代查询过程。
  当客户端向本地 DNS 服务器发出请求后，本地 DNS 服务器查询本机缓存，如果有记录，则直接返回；如果没有，则本地 DNS 服务器以客户端的身份将查询请求发给根名称服务器，这个过程是递归查询过程。根名称服务器通过查询返回给本地 DNS 服务器 .com 顶级名称服务器的 IP 地址；本地 DNS 服务器收到 .com 顶级名称服务器的 IP 地址后继续向.com 顶级名称服务器发出请求，顶级名称服务器收到请求后查询缓存，如果有记录则直接返回本地 DNS 服务器，如果没有，则返回 baidu.com 二级名称服务器的 IP 地址；本地名称服务器继续发出请求，二级名称服务器同样查找缓存返回www.baidu.com的IP地址。(最多127级域)。
- 递归查询
  客户端和本地 DNS 服务器的查询就属于递归查询，客户端发出查询请求后处于等待状态，本地 DNS 以客户端身份询问下一个 DNS 服务器，直到本地 DNS 服务器返回确定回复或否定答复。

一次完整的查询请求经过的流程： Client -->hosts 文件 -->DNS Service Local Cache --> DNS Server (recursion) --> Server Cache --> iteration(迭代) --> 根--> 顶级域名 DNS-->二级域名 DNS…

## DNS 服务器类型

- 主名称服务器(primary name server)：从域管理员构造的本地磁盘文件中加载域信息，该文件（区域文件）包含着该服务器具有管理权的一部分域结构的最精确信息。主服务器是一种权威性服务器，因为它以绝对的权威去回答对其管辖域的任何查询。
- 从名称服务器(secondary name server)：它可从主服务器中复制一整套域信息。区域文件是从主服务器中复制出来的，并作为本地磁盘文件存储在辅助服务器中。这种复制称为”区域文件复制”。在辅助域名服务器中有一个所有域信息的完整拷贝，可以有权威地回答对该域的查询。因此，辅助域名服务器也称作权威性服务器。配置辅助域名服务器不需要生成本地区文件，因为可以从主服务器中下载该区文件。
- 唯高速缓存名称服务器(caching-only server)：可运行域名服务器软件，但是没有域名数据库软件。它从某个远程服务器取得域名服务器查询的结果，一旦取得一个，就将它放在高速缓存中，以后查询相同的信息时就用它予以回答。高速缓存服务器不是权威性服务器，因为它提供的所有信息都是间接信息。当 BIND 被配置为缓存服务器的时候，它只会回应已缓存的请求，并将所有其他的请求转发到上游的 DNS 服务器。缓存名称服务器只需要.这个 zone file 文件即可。

## DNS 配置的主要文件组

- /etc/hosts 　　主机的一个文件列表 　　添加记录如:111.13.100.92 www.baidu.com

对于简单的主机名解析（点分表示法），默认在请求 DNS 或 NIS 网络域名服务器前，/etc/named.conf 通常会告诉程序先查看此文件。

- /etc/resolv.conf 　　转换程序配置文件

在配置程序请求 BIND 域名查询服务查询主机名时，必须告诉程序使用哪个域名服务器和 IP 地址来完成这个任务

- /etc/named.conf 　　 BIND 主文件

设置一般的 name 参数，指向该服务器使用的域数据库的信息源

- /var/named/named.ca 　　根域名配置服务器指向文件

指向根域名配置服务器，用于告诉缓存服务器初始化

- /var/named/localhost.zone 　 localhost 区正向域名解析文件

用于将本地 IP 地址（127.0.0.1）转换为本地回送 IP 地址（127.0.0.1）

- /var/named/name.local 　　 localhost 区反向域名解析文件

用于将 localhost 名字转换为本地回送 IP 地址（127.0.0.1）

- /etc/named.rfc1912.zones 　　区块设置文件

## named.conf 文件的配置

- acl 　　定义 ip 地址的访问控制清单
- control 　　定义 rndc 使用的控制通道
- include 　　把其他的文件包含到配置文件中
- key 　　定义授权的安全密钥
- logging 　　定义日志内容和位置
- options 　　定义全局配置选项和默认值
- server 　　定义远程服务的特征
- zone 　　定义一个区

## DNS 的资源记录（Resource Record, RR）格式

DNS 域名数据库有资源记录和区文件指令组成，由 SOA（Start Of Authority 起始授权机构记录，SOA 记录说明了在众多 NS 记录里那一台才是主名称服务器。） RR 开始，同时包括 NS RR；正向解析文件包括 A （internet Address，作用，FQDN --> IP）RR MX （Mail eXchanger，邮件交换器）RR 和 CNAME（Canonical NAME 别名） RR 等；反向解析文件包括 PTR（PTR: PoinTeR，IP --> FQDN） RR

- RR 语法：name 　　[TTL]　　 IN 　　 type 　　 value （字段之间由空格和制表符隔开）

注意： (1) TTL 可从全局继承　　(2) @可用于引用当前区域的名字 　　(3) 同一个名字可以通过多条记录定义多个不同的值；此时 DNS 服务器会以轮询方式响应 　　(4) 同一个值也可能有多个不同的定义名字；通过多个不同的 名字指向同一个值进行定义；此仅表示通过多个不同的名字 可以找到同一个主机

- SOA 记录：name: 当前区域的名字，例如“heiye.com.” 　　 value: 有多部分组成 (1) 当前区域的主 DNS 服务器的 FQDN，也可以使用当前区域的名字； (2) 当前区域管理员的邮箱地址；地址中不能使用@符号，一般用.替换 如 linuxedu.heiye.com (3) 主从服务区域传输相关定义以及否定的答案的统一的 TTL

```
例如： heiye.com.　　86400 　　IN 　　SOA 　　ns.heiye.com.

　　　 nsadmin.heiye.com. 　　(

　　　 　　　　2015042201 ;

　　　　　　　 序列号 2H ;

　　　　　　　 刷新时间 10M ;

　　　　　　　 重试时间 1W ;

　　　　　　　 过期时间 1D ;

　　　　　　　 否定答案的TTL值

　　　)
```

- NS 记录：name: 当前区域的名字 　　 value: 当前区域的某 DNS 服务器的名字，例如 ns.heiye.com. 注意：一个区域可以有多个 NS 记录

```
例如：heiye.com. 　　IN 　　NS　　ns1.heiye.com.

　　　heiye.com. 　　IN 　　NS  ]　ns2.heiye.com.
```

注意： (1) 相邻的两个资源记录的 name 相同时，后续的可省略 (2) 对 NS 记录而言，任何一个 ns 记录后面的服务器名字 ，都应该在后续有一个 A 记录

- MX 记录（Mail eXchanger）：name: 当前区域的名字 　　 value: 当前区域的某邮件服务器(smtp 服务器)的主机名 ， 一个区域内，MX 记录可有多个；

```
但每个记录的value之前应 该有一个数字(0-99)，表示此服务器的优先级；数字越小优 先级越高  例如：

heiye.com. 　　IN 　　MX 　　10 　　mx1.heiye.com.

　　　　　　　 IN 　　MX 　　20 　　mx2.heiye.com.
```

注意： (1) 对 MX 记录而言，任何一个 MX 记录后面的服务器名字 ，都应该在后续有一个 A 记录

- A 记录（Addrss）:name: 某主机的 FQDN，例如www.heiye.com. 　　 value: 主机名对应主机的 IP 地址

```
例如： www.heiye.com. 　　IN 　　A 　　1.1.1.1 　　

　　　 www.heiye.com.　　 IN 　　A 　　2.2.2.2 　　

　　　 mx1.heiye.com. 　　IN 　　A 　　3.3.3.3

　　　 mx2.heiye.com.    　 IN 　　A 　　4.4.4.4

　　　 *.heiye.com. 　　　  IN 　　A 　　5.5.5.5

　　　 heiye.com. 　　　　IN 　　A 　　 6.6.6.6 　　

避免用户写错名称时给错误答案，可通过泛域名解析进行解 析至某特定地址
```

- 其他记录：AAAA: name: FQDN 　　 value: IPv6

- PTR: name: IP，有特定格式，把 IP 地址反过来写，1.2.3.4，要写 作 4.3.2.1；而有特定后缀：in-addr.arpa.，所以完整写法为 ：4.3.2.1.in-addr.arpa. 　　 value: FQDN

```
例如： 4.3.2.1.　　in-addr.arpa. 　　IN 　　PTR 　　www.heiye.com.

如1.2.3为网络地址，可简写成： 4 　　IN 　　PTR 　　www.heiye.com.
```

注意：网络地址及后缀可省略；主机地址依然需要反着写

- 别名记录:name: 别名的 FQDN 　　 value: 真正名字的 FQDN

```
例如： www.heiye.com. 　　IN 　　CNAME 　　websrv.heiye.com.
```

named 字段：

（1）根域以” . “结束，并且只有一个，没有上级域。而在 Internet 中，根域一般不需要表现出来。

（2）@：默认域，文件使用$ORIGIN domain 来说明默认域。

（3）ttl 全称”Time to Live “，以秒为单位记录该资源记录中存放高速缓存中的时间长度。通常此处设为空，表示采用 SOA 的最小 ttl 值。

（4）IN：将该记录标志为一个 Internet DNS 资源记录。

type 字段:

(1)A 记录：主机名对应的 IP 地址记录，用户可将该域名下网站服务器指向自己的 Web 服务器，同时也可设置域名的二级域名。

(2)MX 记录：邮件交换记录可将该域下所有邮件服务器 指向自己的邮件服务器，只需在线填写服务器的 IP 地址。

(3)CNAME 记录：别名记录，可允许多个名字映射到同一计算机，通常用于同时提供 Web 和邮件服务器的计算机。

(4)SOA 记录：一个授权区的开始，配置文件的第一个记录必须是 SOA 的开始。

(5)PTR 记录：用于地址到主机名的映射。

(6)HINFO 记录：由一组描述主机的信息文件组成，通常包括硬件名称和操作系统名称。

value 字段：

（1）A :存放 IP 地址。

（2）CNAME：设置主机别名。

（3）HINFO：通常为两行，分别对应 Hareware（计算机硬件名称）和 OS-type（操作系统名称）。

（4）NS：域名服务器的名称。

（5）PTR:主机真实名称。

测试检查配置文件错误的工具：nslookup、dig、named-checkzone、host、named-checkconf 及 dlint。

## DNS 服务器和客户端配置

- 安装 DNS 程序：`yum -y install bind bind-chroot bind-utils`

BIND 全称为 Berkeley Internet Name Domain（伯克利因特网名称域系统），BIND 主要有三个版本：BIND4、BIND8、BIND9。BIND8 版本融合了许多提高效率、稳定性和安全性的技术，而 BIND9 增加了一些超前的理念，例如 IPv6 支持、密钥加密、多处理器支持、线程安全操作、增量区传送等。

bind 包里包含了 DNS 服务的主程序包。

bind-chroot 包是 bind 的一个功能包，使 bind 可以在 chroot 模式下运行。也就是说 bind 运行的主目录并不是系统的根目录，只是系统的一个子目录而已。这是为了提高安全性，使得访问的范围仅限于这个子目录。

bind-utils 包是客户端工具，系统默认安装，用于搜索域名指令。

注意： (1) 一台物理服务器可同时为多个区域提供解析 (2) 必须要有根区域文件`named.ca` (3) 应该有两个（如果包括 ipv6 的，应该更多）实现 localhost 和本地回环地址的解析库

- DNS 相关配置文件

/etc/named.conf 是 BIND 的核心配置文件，它包含了 BIND 的基本配置，但其并不包括区域数据。

/var/named/目录是 DNS 数据库文件存放目录，每一个域文件都放在这里。

注意：DNS 服务器名是 named，端口 53 用于客户端查询，端口 953 用于 DNS 主从同步

```
systemctl start named
systemctl enable named
netstat -antup | grep named
tcp        0      0 127.0.0.1:53            0.0.0.0:*               LISTEN      2347/named
tcp        0      0 127.0.0.1:953           0.0.0.0:*               LISTEN      2347/named
tcp6       0      0 ::1:53                  :::*                    LISTEN      2347/named
tcp6       0      0 ::1:953                 :::*                    LISTEN      2347/named
udp        0      0 127.0.0.1:53            0.0.0.0:*                           2347/named
udp6       0      0 ::1:53                  :::*                                2347/named
```

- 配置文件参数
  在/etc/named.conf 中大致分为针对全局生效，存放常规设置的 options 部分；以及针对某个区域，由各个区域设置的 zone 部分。另外还有 logging（日志选项）和 acl（访问控制列表）可选。

去除注释后的/etc/named.conf 配置文件如下：

```
options {
//指定BIND通过哪些网络接口和哪个端口来接收客户端请求。如果省略端口号，则使用默认端口53。127.0.0.1允许接收来自本地的请求。如果完全省略此项或网络接口是any，则默认使用所有端口。
        listen-on port 53 { 127.0.0.1; };
//指定BIND通过哪个端口监听IPv6客户端请求。IPv6，服务器只接受通配符地址，any和none。
        listen-on-v6 port 53 { ::1; };
//BIND可以在该目录中找到包含区域数据的文件
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
//定义客户端可以自此发送DNS请求的网络。可以用地址信息（192.168.5.101/24）替换或使用any替换
        allow-query     { localhost; };
        recursion yes;
        dnssec-enable yes;
        dnssec-validation yes;
        bindkeys-file "/etc/named.iscdlv.key";
        managed-keys-directory "/var/named/dynamic";
        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;　　//指定区域类型
        file "named.ca";　　//区域文件，需要手动创建。范例文件为named.localhost
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

type 字段指定区域的类型，对于区域的管理至关重要，一共分为 5 种：

master：主 DNS 服务器，拥有区域数据文件，并对此区域提供管理数据。

slave：辅助 DNS 服务器，拥有主 DNS 服务器的区域数据文件的副本，服务器 DNS 服务器会从主 DNS 服务器同步所有区域数据。

stub：stub 区域与 slave 区域类似，但只复制主 DNS 服务器上的 NS 记录，而不像 slave 会复制所有区域数据

forward：转发配置域

hint：根域服务器的初始化使用的参数

- 搭建内网 DNS 服务器

  - 备份 DNS 服务器的配置文件/etc/named.conf `cp /etc/named.conf /etc/named.conf.bak`
  - 修改 DNS 服务器的配置文件/etc/named.conf

  ```
  options {
        listen-on port 53 { 172.16.20.1; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        allow-query     { any; };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        /* Path to ISC DLV key */
        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
  };

  logging {
          channel default_debug {
                  file "data/named.run";
                  severity dynamic;
          };
  };

  include "/etc/named.rfc1912.zones";
  include "/etc/named.root.key";

  ```

  - 添加 olavoice.com zone `vi more /etc/named.rfc1912.zones`

  ```
   zone "localhost.localdomain" IN {
          type master;
          file "named.localhost";
          allow-update { none; };
  };

  zone "localhost" IN {
          type master;
          file "named.localhost";
          allow-update { none; };
  };

  zone "1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
          type master;
          file "named.loopback";
          allow-update { none; };
  };

  zone "1.0.0.127.in-addr.arpa" IN {
          type master;
          file "named.loopback";
          allow-update { none; };
  };

  zone "0.in-addr.arpa" IN {
          type master;
          file "named.empty";
          allow-update { none; };
  };

  zone "olavoice.com" IN {
          type master;
          file "named.olavoice.com";
          allow-update { none; };
  };

  ```

  - 创建区域文件，修改权限，并进行修改 `cp -a /var/named/named.localhost /var/named/named.olavoice.com`

  ```
  $TTL 1D  //设置有效地址解析记录的默认缓存时间，默认为1天
  //olavoice.com.也可以使用@，表示当前的域
  @       IN SOA  @ rname.invalid. (
  //更新序列号，用于标示数据库的变换，可以在10位以内。如果存在辅助DNS区域，建议每次更新完数据库，手动加1
                                          0       ; serial
  //刷新时间，从域名服务器更新该地址数据库文件的间隔时间，默认为1天
                                          1D      ; refresh
  //重试延时，从域名服务器更新地址数据库失败后，等待多长时间，默认为1小时
                                          1H      ; retry
  //到期失效时间，超过改时间人无法更新地址数据库，那么不再尝试，默认为1周
                                          1W      ; expire
  //无效地址解析记录默认缓存时间（该数据库中不存在的地址），默认为3小时。
                                          3H )    ; minimum
          NS      @
          A       127.0.0.1
          AAAA    ::1

  api-i A 172.16.20.1
  solr A 172.16.20.27
  proxy A 172.16.11.14
  maven A 172.16.11.14
  mail A 172.16.20.31

  redis A 172.16.20.65
  1.solr A 172.16.20.26
  2.solr A 172.16.20.46
  3.solr A 172.16.20.48
  ```

  - 检查配置文件 `named-checkzone olavoice.com /var/named/named.olavoice.com`
  - 重启 DNS 服务器 `systemctl restart named`

- 配置客户端服务器的 DNS 解析地址为，上述 dns server 服务器的 ip 地址，必须保证 dns server 的 53 端口能正常访问
