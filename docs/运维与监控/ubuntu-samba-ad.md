# Ubuntu 20.04 Samba 使用 Active Directory 登录认证

## 测试机环境
- Windows Server 2019
  - IP: 192.168.132.130
  - Hostname: ad.rikyli.com
  - AD: rikyli.com
- Ubuntu 20.04
  - IP: 192.168.132.129
  - Hostname: ubuntu2004.rikyli.com
  - dns: 192.168.132.130

## 安装

### [Windows Server 2019 上安装配置AD域参考](https://www.jb51.net/server/296532l26.htm#_lab2_0_0)

- 运行“dnsmgmt.msc”打开DNS管理器，右击选择属性，切换的转发器,添加外部DNS Server，否则无法解析外部dns
![dns 转发](https://gitee.com/rikylee/images/raw/master/image/202403061555142.png)

- 添加ubutuntu2004的dns记录
![dns 解析](https://gitee.com/rikylee/images/raw/master/image/202403061555477.png)

### Ubuntu 20.04 加入域(rikyli.com)

**注意:**
  - ubuntu2004 的时区需要修改为和 windows server 的时区一致
  - ubuntu2004 的 DNS 需要修改为 windows server 的 DNS 地址

- 安装加域需要用到的软件包
```bash
olami@ubuntu2004:~$ sudo apt install -y realmd sssd-ad sssd-tools adcli
```

- 搜索需要加入的AD域
```bash
olami@ubuntu2004:~$ sudo realm discover -v rikyli.com

* Resolving: _ldap._tcp.rikyli.com
* Performing LDAP DSE lookup on: 192.168.132.130
* Successfully discovered: rikyli.com
rikyli.com
  type: kerberos
  realm-name: RIKYLI.COM
  domain-name: rikyli.com
  configured: no
  server-software: active-directory
  client-software: sssd
  required-package: sssd-tools
  required-package: sssd
  required-package: libnss-sss
  required-package: libpam-sss
  required-package: adcli
  required-package: samba-common-bin
```
- 加入域,默认使用administrator 账号认证;也可使用具有管理员身份的账号认证
```bash
olami@ubuntu2004:~$ sudo realm join -v rikyli.com

* Resolving: _ldap._tcp.rikyli.com
* Performing LDAP DSE lookup on: 192.168.132.130
* Successfully discovered: rikyli.com
Password for Administrator:
* Unconditionally checking packages
* Resolving required packages
* LANG=C /usr/sbin/adcli join --verbose --domain rikyli.com --domain-realm RIKYLI.COM --domain-controller 192.168.132.130 --login-type user --login-user Administrator --stdin-password
* Using domain name: rikyli.com
* Calculated computer account name from fqdn: UBUNTU2004
* Using domain realm: rikyli.com
* Sending NetLogon ping to domain controller: 192.168.132.130
* Received NetLogon info from: ad.rikyli.com
* Wrote out krb5.conf snippet to /var/cache/realmd/adcli-krb5-QoYDgI/krb5.d/adcli-krb5-conf-Rc9A4H
* Authenticated as user: <Administrator@RIKYLI.COM>
* Using GSS-SPNEGO for SASL bind
* Looked up short domain name: RIKYLI
* Looked up domain SID: S-1-5-21-2820864518-3051917984-981434457
* Using fully qualified name: ubuntu2004.rikyli.com
* Using domain name: rikyli.com
* Using computer account name: UBUNTU2004
* Using domain realm: rikyli.com
* Calculated computer account name from fqdn: UBUNTU2004
* Generated 120 character computer password
* Using keytab: FILE:/etc/krb5.keytab
* Computer account for UBUNTU2004$ does not exist
* Found well known computer container at: CN=Computers,DC=rikyli,DC=com
* Calculated computer account: CN=UBUNTU2004,CN=Computers,DC=rikyli,DC=com
* Encryption type [3] not permitted.
* Encryption type [1] not permitted.
* Created computer account: CN=UBUNTU2004,CN=Computers,DC=rikyli,DC=com
* Sending NetLogon ping to domain controller: 192.168.132.130
* Received NetLogon info from: ad.rikyli.com
* Set computer password
* Retrieved kvno '2' for computer account in directory: CN=UBUNTU2004,CN=Computers,DC=rikyli,DC=com
* Checking RestrictedKrbHost/ubuntu2004.rikyli.com
* Added RestrictedKrbHost/ubuntu2004.rikyli.com
* Checking RestrictedKrbHost/UBUNTU2004
* Added RestrictedKrbHost/UBUNTU2004
* Checking host/ubuntu2004.rikyli.com
* Added host/ubuntu2004.rikyli.com
* Checking host/UBUNTU2004
* Added host/UBUNTU2004
* Discovered which keytab salt to use
* Added the entries to the keytab: UBUNTU2004$@RIKYLI.COM: FILE:/etc/krb5.keytab
* Added the entries to the keytab: host/UBUNTU2004@RIKYLI.COM: FILE:/etc/krb5.keytab
* Added the entries to the keytab: host/ubuntu2004.rikyli.com@RIKYLI.COM: FILE:/etc/krb5.keytab
* Added the entries to the keytab: RestrictedKrbHost/UBUNTU2004@RIKYLI.COM: FILE:/etc/krb5.keytab
* Added the entries to the keytab: RestrictedKrbHost/ubuntu2004.rikyli.com@RIKYLI.COM: FILE:/etc/krb5.keytab
* /usr/sbin/update-rc.d sssd enable
* /usr/sbin/service sssd restart
* Successfully enrolled machine in realm
```

在AD 主机上运行“dsa.msc” 可以在此界面查看和管理域`riky.com`中的用户、组和服务等

![AD 主机信息](https://gitee.com/rikylee/images/raw/master/image/202403061555978.png)

- 通过搜索域账号，查看加域是否成功
```bash
olami@ubuntu2004:~$ id rikytest1@rikyli.com
uid=503401118(rikytest1) gid=503400513(domain users) groups=503400513(domain users),503401105(vt)
```
- 查看加入的域
```bash
olami@ubuntu2004:~$ realm list
rikyli.com
  type: kerberos
  realm-name: RIKYLI.COM
  domain-name: rikyli.com
  configured: kerberos-member
  server-software: active-directory
  client-software: sssd
  required-package: sssd-tools
  required-package: sssd
  required-package: libnss-sss
  required-package: libpam-sss
  required-package: adcli
  required-package: samba-common-bin
  login-formats: %U
  login-policy: allow-realm-logins
```

- 修改sssd.conf，使域账号登录不用输入@后缀
```bash
olami@ubuntu2004:~$ sudo vi /etc/sssd/sssd.conf

[sssd]
domains = rikyli.com
config_file_version = 2
services = nss, pam

[domain/rikyli.com]
default_shell = /bin/bash
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = RIKYLI.COM
realmd_tags = manages-system joined-with-adcli
id_provider = ad

# fallback_homedir = /home/%u@%d
# 修改homedir 不包含domain
fallback_homedir = /home/%u
ad_domain = rikyli.com

# use_fully_qualified_names = True
# 修改不使用完整名称，不用输入@后缀
use_fully_qualified_names = False
ldap_id_mapping = True

access_provider = ad
# 使用简单访问控制情况下，配合下面两项配置可以只允许特定组或者成员登录系统
# access_provider = simple

#允许vt组成员登录
# simple_allow_groups = vt
#允许单独的用户via-test登录
# simple_allow_users = via-test


#不强制应用组策略，登录加入Active Directory域的系统时，将默认尝试应用组策略。在某些情况下，如果缺少特定策略，登录将被拒绝。
# ad_gpo_access_control = permissive

olami@ubuntu2004:~$ sudo systemctl restart sssd

```

- 第一次使用域账号登录时，自动创建用户目录
```bash
olami@ubuntu2004:~$ sudo pam-auth-update --enable mkhomedir
```

- 创建共享目录
```bash
olami@ubuntu2004:~$ sudo mkdir -p -m 1777 /data/share1
olami@ubuntu2004:~$ sudo mkdir -p -m 1777 /data/share2
```

- 安装samba
```bash
olami@ubuntu2004:~$ sudo apt install -y  samba  samba-client
olami@ubuntu2004:~$ sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.bak
```

-  修改默认配置文件
```bash
olami@ubuntu2004:~$ sudo vi  /etc/samba/smb.conf

[global]
  # 添加域和认证配置
   workgroup = rikyli

   client signing = yes

   client use spnego = yes

   kerberos method = secrets and keytab

   security = ads

   realm = rikyli.com

  # 以下为系统默认配置
   server string = %h server (Samba, Ubuntu)

   log file = /var/log/samba/log.%m

   max log size = 1000

   logging = file

   panic action = /usr/share/samba/panic-action %d

   server role = standalone server

   obey pam restrictions = yes

   unix password sync = yes

   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:*%n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .

   pam password change = yes

   map to guest = bad user

# 添加共享目录配置，只允许vt组成员和group2组成员、via-test用户访问
[share1]
comment = Ad user share files
path = /data/share1
writeable = yes
guest ok = no
valid users = @RIKYLI\vt, @RIKYLI\group2, RIKYLI\rikytest2
create mask = 0755
directory mask = 0755

# 添加共享目录配置允许所有域用户访问
[share2]
comment = common user share files
path = /data/share2
writeable = yes
browseable = yes
guest ok = yes
create mask = 0755
directory mask = 0755

olami@ubuntu2004:~$ sudo systemctl restart smbd
```
- samba 加入域(rikyli.com)
```bash
olami@ubuntu2004:~$ sudo net ads join -U via-test
Unknown parameter encountered: "client spnego"
Ignoring unknown parameter "client spnego"
Password for [RIKYLI\via-test]:
Using short domain name -- RIKYLI
Joined 'UBUNTU2004' to dns domain 'rikyli.com'
No DNS domain configured for ubuntu2004. Unable to perform DNS Update.
DNS update failed: NT_STATUS_INVALID_PARAMETER
```
- 测试samba共享目录,出现异常，安装winbind
```bash
olami@ubuntu2004:~$ smbclient //ubuntu2004.rikyli.com/share2 -U my
lpcfg_do_global_parameter: WARNING: The "client use spnego" option is deprecated
Password for [RIKYLI\my]:
session setup failed: NT_STATUS_NO_LOGON_SERVERS

olami@ubuntu2004:~$ more /var/log/samba/log.192.168.132.130
[2024/03/06 14:53:24.182442,  0] ../../source3/auth/auth_generic.c:126(auth3_generate_session_info_pac)
  auth3_generate_session_info_pac: winbindd not running - but required as domain member: NT_STATUS_NO_LOGON_SERVERS
[2024/03/06 14:53:24.201900,  0] ../../source3/auth/auth_generic.c:126(auth3_generate_session_info_pac)
  auth3_generate_session_info_pac: winbindd not running - but required as domain member: NT_STATUS_NO_LOGON_SERVERS
[2024/03/06 14:53:24.221636,  0] ../../source3/auth/auth_generic.c:126(auth3_generate_session_info_pac)
  auth3_generate_session_info_pac: winbindd not running - but required as domain member: NT_STATUS_NO_LOGON_SERVERS
[2024/03/06 14:53:24.240753,  0] ../../source3/auth/auth_generic.c:126(auth3_generate_session_info_pac)
  auth3_generate_session_info_pac: winbindd not running - but required as domain member: NT_STATUS_NO_LOGON_SERVERS

olami@ubuntu2004:~$ sudo apt install winbind -y
Reading package lists... Done
Building dependency tree
Reading state information... Done
Suggested packages:
  libnss-winbind libpam-winbind
The following NEW packages will be installed:
  winbind
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 455 kB of archives.
After this operation, 2,011 kB of additional disk space will be used.
Get:1 <http://mirrors.tuna.tsinghua.edu.cn/ubuntu> focal-updates/main amd64 winbind amd64 2:4.15.13+dfsg-0ubuntu0.20.04.7 [455 kB]
Fetched 455 kB in 1s (574 kB/s)
Selecting previously unselected package winbind.
(Reading database ... 74831 files and directories currently installed.)
Preparing to unpack .../winbind_2%3a4.15.13+dfsg-0ubuntu0.20.04.7_amd64.deb ...
Unpacking winbind (2:4.15.13+dfsg-0ubuntu0.20.04.7) ...
Setting up winbind (2:4.15.13+dfsg-0ubuntu0.20.04.7) ...
mkdir: created directory '/var/lib/samba/winbindd_privileged'
changed group of '/var/lib/samba/winbindd_privileged' from root to winbindd_priv
mode of '/var/lib/samba/winbindd_privileged' changed from 0755 (rwxr-xr-x) to 0750 (rwxr-x---)
Created symlink /etc/systemd/system/multi-user.target.wants/winbind.service → /lib/systemd/system/winbind.service.
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.14) ...
Processing triggers for systemd (245.4-4ubuntu3.23) ...
```
- 测试samba共享目录
```bash
olami@ubuntu2004:/data/share1$ smbclient //ubuntu2004.rikyli.com/share1 -U rikytest3
lpcfg_do_global_parameter: WARNING: The "client use spnego" option is deprecated
Password for [RIKYLI\rikytest3]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Wed Mar  6 15:05:35 2024
  ..                                  D        0  Wed Mar  6 14:47:45 2024
  aba.txt                             N        5  Wed Mar  6 15:05:35 2024

                20463184 blocks of size 1024. 12457828 blocks available
smb: \>
```
- 在AD主机上添加用户，测试能否正常访问samba共享目录

- 新增本地用户和用户组sambaw
```bash
olami@ubuntu2004:~$ sudo groupadd sambaw
[sudo] password for olami:
olami@ubuntu2004:~$ sudo useradd rikyts
olami@ubuntu2004:~$ sudo gpasswd -a rikyts sambaw
Adding user rikyts to group sambaw

olami@ubuntu2004:~$ id rikyts
uid=1001(rikyts) gid=1002(rikyts) groups=1002(rikyts),1001(sambaw)
```
- 修改samba配置，允许本地用户组sambaw访问共享目录share1
```bash

olami@ubuntu2004:~$ sudo vi  /etc/samba/smb.conf

[global]
  # 添加域和认证配置
   workgroup = rikyli

   client signing = yes

   client use spnego = yes

   kerberos method = secrets and keytab

   security = ads

   realm = rikyli.com

  # 以下为系统默认配置
   server string = %h server (Samba, Ubuntu)

   log file = /var/log/samba/log.%m

   max log size = 1000

   logging = file

   panic action = /usr/share/samba/panic-action %d

   server role = standalone server

   obey pam restrictions = yes

   unix password sync = yes

   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:*%n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .

   pam password change = yes

   map to guest = bad user

# 添加共享目录配置，只允许vt组成员和group2组成员、via-test用户、本地用户组sammbaw访问
[share1]
comment = Ad user share files
path = /data/share1
writeable = yes
guest ok = no
valid users = @RIKYLI\vt, @RIKYLI\group2, RIKYLI\rikytest2, @sambaw
create mask = 0755
directory mask = 0755

# 添加共享目录配置允许所有域用户访问
[share2]
comment = common user share files
path = /data/share2
writeable = yes
browseable = yes
guest ok = yes
create mask = 0755
directory mask = 0755

olami@ubuntu2004:~$ sudo systemctl restart smbd

olami@ubuntu2004:~$ smbclient //ubuntu2004.rikyli.com/share1 -U \\rikyts
lpcfg_do_global_parameter: WARNING: The "client use spnego" option is deprecated
Password for [rikyts]:
Try "help" to get a list of possible commands.
smb: \>
smb: \> ls
  .                                   D        0  Wed Mar  6 15:05:35 2024
  ..                                  D        0  Wed Mar  6 14:47:45 2024
  aba.txt                             N        5  Wed Mar  6 15:05:35 2024

                20463184 blocks of size 1024. 12420768 blocks available
smb: \> exit
```


