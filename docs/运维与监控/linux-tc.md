# Linux TC

## TC 原理介绍

Linux 操作系统中的流量控制器 TC（Traffic Control）用于 Linux 内核的流量控制，主要是通过在输出端口处建立一个队列来实现流量控制

Linux 流量控制的基本原理如下图所示

接收包从输入接口（Input Interface）进来后，经过流量限制（Ingress Policing）丢弃不符合规定的数据包，由输入多路分配器（Input De-Multiplexing）进行判断选择：如果接收包的目的是本主机，那么将该包送给上层处理；否则需要进行转发，将接收包交到转发块（Forwarding Block）处理。转发块同时也接收本主机上层（TCP、UDP 等）产生的包。转发块通过查看路由表，决定所处理包的下一跳。然后，对包进行排列以便将它们传送到输出接口（Output Interface）。一般我们只能限制网卡发送的数据包，不能限制网卡接收的数据包，所以我们可以通过改变发送次序来控制传输速率。Linux 流量控制主要是在输出接口排列时进行处理和实现的

## 流量控制方式

- SHAPING(限制) 当流量被限制，它的传输速率就被控制在某个值以下。限制值可以大大小于有效带宽，这样可以平滑突发数据流量，使网络更为稳定。shaping（限制）只适用于向外的流量

- SCHEDULING(调度) 通过调度数据包的传输，可以在带宽范围内，按照优先级分配带宽。SCHEDULING(调度)也只适于向外的流量

- POLICING(策略) SHAPING 用于处理向外的流量，而 POLICIING(策略)用于处理接收到的数据

- DROPPING(丢弃) 如果流量超过某个设定的带宽，就丢弃数据包，不管是向内还是向外

## 流量控制处理对象

流量的处理由三种对象控制，它们是：qdisc(排队规则)、class(类别)和 filter(过滤器)

- QDISC(排队规则) 是 queueing discipline 的简写，它是理解流量控制(traffic control)的基础。无论何时，内核如果需要通过某个网络接口发送数据包，它都需要按照为这个接口配置的 qdisc(排队规则)把数据包加入队列。然后，内核会尽可能多地从 qdisc 里面取出数据包，把它们交给网络适配器驱动模块。最简单的 QDisc 是 pfifo 它不对进入的数据包做任何的处理，数据包采用先入先出的方式通过队列。不过，它会保存网络接口一时无法处理的数据包,QDISC 的类别如下：

  - CLASSLESS QDISC 系统自行优先处理的队列
  - CLASSFUL QDISC 一般使用 TC 限速使用这些队列

- CLASS(类) 某些 QDisc(排队规则)可以包含一些类别，不同的类别中可以包含更深入的 QDISC(排队规则)，通过这些细分的 QDisc 还可以为进入的队列的数据包排队。通过设置各种类别数据包的离队次序，QDisc 可以设置网络数据流量的优先级

- FILTER(过滤器) Filter(过滤器)用于为数据包分类，决定它们按照何种 QDisc 进入队列。无论何时数据包进入一个划分子类的类别中，都需要进行分类。分类的方法可以有多种，使用 fileter(过滤器)就是其中之一。使用 Filter(过滤器)分类时，内核会调用附属于这个类(class)的所有过滤器，直到返回一个判决。如果没有判决返回，就作进一步的处理，而处理方式和 QDisc 有关。需要注意的是，Filter(过滤器)是在 QDisc 内部，它们不能作为主体

  - u32 Generic filtering on arbitrary packet data, assisted by syntax to abstract common operations.See `man tc-u32`for details
  - route Filter packets based on routing table. See `tc-route` for details
  - tcindex Filter packets based on traffic control index. See `man tc-tcindex`
  - matchall Traffic control filter that matches every packet. See `man tc-matchall` for details

### CLASSLESS QDISC

CLASSLESS QDISC 包含如下：

- [p|b]fifo 使用最简单的 qdisc，纯粹的先进先出。只有一个参数：limit，用来设置队列的长度,pfifo 是以数据包的个数为单位；bfifo 是以字节数为单位

- pfifo_fast 在编译内核时，如果打开了高级路由器(Advanced Router)编译选项，pfifo_fast 就是系统的标准 QDISC。它的队列包括三个波段(band)。在每个波段里面，使用先进先出规则。而三个波段(band)的优先级也不相同，band 0 的优先级最高，band 2 的最低。如果 band 里面有数据包，系统就不会处理 band 1 里面的数据包，band 1 和 band 2 之间也是一样。数据包是按照服务类型(Type of Service,TOS)被分配多三个波段(band)里面的

- red 是 Random Early Detection(随机早期探测)的简写。如果使用这种 QDISC，当带宽的占用接近于规定的带宽时，系统会随机地丢弃一些数据包。它非常适合高带宽应用

- sfq 是 Stochastic Fairness Queueing 的简写。它按照会话(session--对应于每个 TCP 连接或者 UDP 流)为流量进行排序，然后循环发送每个会话的数据包

- tbf 是 Token Bucket Filter 的简写，适合于把流速降低到某个值

CLASSLESS QDISC 配置

如果没有可分类 QDisc，CLASSLESS QDISC 只能附属于设备的根。它们的用法: `tc qdisc add dev DEV root QDISC QDISC-PARAMETERS`

要删除一个不可分类 QDisc，需要使用命令: `tc qdisc del dev DEV root`

一个网络接口上如果没有设置 QDISC，pfifo_fast 就作为缺省的 QDISC

### CLASSFUL QDISC

CLASSFUL QDISC 包括

- CBQ 是 Class Based Queueing(基于类别排队)的缩写。它实现了一个丰富的连接共享类别结构，既有限制(shaping)带宽的能力，也具有带宽优先级管理的能力。带宽限制是通过计算连接的空闲时间完成的。空闲时间的计算标准是数据包离队事件的频率和下层连接(数据链路层)的带宽

  ```shell
  tc qdisc ... dev dev ( parent classid | root) [ handle major: ] cbq [ allot bytes ] avpkt bytes bandwidth rate [ cell bytes ] [ ewma log ] [ mpu bytes ]

  tc  class ... dev dev parent major:[minor] [ classid major:minor ] cbq allot bytes [ bandwidth rate ] [ rate rate ] prio priority [ weight weight ] [ minburst packets ] [ maxburst packets ] [ ewma log ] [ cell bytes ] avpkt bytes [ mpu bytes ]
  [ bounded isolated ] [ split handle & defmap defmap ] [ estimator interval timeconstant ]
  ```

- HTB 是 Hierarchy Token Bucket 的缩写。通过在实践基础上的改进，它实现了一个丰富的连接共享类别体系。使用 HTB 可以很容易地保证每个类别的带宽，虽然它也允许特定的类可以突破带宽上限，占用别的类的带宽。HTB 可以通过 TBF(Token Bucket Filter)实现带宽限制，也能够划分类别的优先级

  ```shell
  tc qdisc ... dev dev ( parent classid | root) [ handle major: ] htb [ default minor-id ]

  tc class ... dev dev parent major:[minor] [ classid major:minor ] htb rate rate [ ceil rate ] burst bytes [ cburst bytes ] [ prio priority ]
  ```

- PRIO QDisc 不能限制带宽，因为属于不同类别的数据包是顺序离队的。使用 PRIO QDisc 可以很容易对流量进行优先级管理，只有属于高优先级类别的数据包全部发送完毕，才会发送属于低优先级类别的数据包。为了方便管理，需要使用 iptables 或者 ipchains 处理数据包的服务类型(Type Of Service,ToS)

## 操作原理

类(Class)组成一个树，每个类都只有一个父类，而一个类可以有多个子类。某些 QDisc(例如：CBQ 和 HTB)允许在运行时动态添加类，而其它的 QDisc(例如：PRIO)不允许动态建立类。允许动态添加类的 QDisc 可以有零个或者多个子类，由它们为数据包排队。此外，每个类都有一个叶子 QDisc，默认情况下，这个叶子 QDisc 使用 pfifo 的方式排队，我们也可以使用其它类型的 QDisc 代替这个默认的 QDisc。而且，这个叶子 QDisc 也可以分类，不过每个子类只能有一个叶子 QDisc。 当一个数据包进入一个分类 QDisc，它会被归入某个子类。我们可以使用以下三种方式为数据包归类，不过不是所有的 QDisc 都能够使用这三种方式

- tc 过滤器(tc filter) 如果过滤器附属于一个类，相关的指令就会对它们进行查询。过滤器能够匹配数据包头所有的域，也可以匹配由 ipchains 或者 iptables 做的标记

- 服务类型(Type of Service) 某些 QDisc 有基于服务类型（Type of Service,ToS）的内置的规则为数据包分类

- skb->priority 用户空间的应用程序可以使用 SO_PRIORITY 选项在 skb->priority 域设置一个类的 ID。

树的每个节点都可以有自己的过滤器，但是高层的过滤器也可以直接用于其子类。如果数据包没有被成功归类，就会被排到这个类的叶子 QDisc 的队中。相关细节在各个 QDisc 的手册页中

## 命名规则

所有的 QDisc、类和过滤器都有 ID。ID 可以手工设置，也可以有内核自动分配。ID 由一个主序列号和一个从序列号组成，两个数字用一个冒号分开

- QDISC 一个 QDisc 会被分配一个主序列号，叫做句柄(handle)，然后把从序列号作为类的命名空间。句柄采用像 10:一样的表达方式。习惯上，需要为有子类的 QDisc 显式地分配一个句柄

- 类(CLASS) 在同一个 QDisc 里面的类分享这个 QDisc 的主序列号，但是每个类都有自己的从序列号，叫做类识别符(classid)。类识别符只与父 QDisc 有关，和父类无关。类的命名习惯和 QDisc 的相同

- 过滤器(FILTER) 过滤器的 ID 有三部分，只有在对过滤器进行散列组织才会用到。详情请参考 tc-filters 手册页

## 单位

tc 命令的所有参数都可以使用浮点数，可能会涉及到以下计数单位

- 带宽或者流速单位

  ```txt
  kbps              千字节／秒
  mbps              兆字节／秒
  kbit              KBits／秒
  mbit              MBits／秒
  bps|一个无单位数字  字节数／秒
  ```

- 数据的数量单位

  ```txt
  kb|k              千字节
  mb|m              兆字节
  mbit              兆bit
  kbit              千bit
  b|一个无单位数字    字节数
  ```

- 时间的计量单位

  ```txt
  s|sec|secs                  秒
  ms|msec|msecs               分钟
  us|usec|usecs|一个无单位数字  微秒
  ```

## TC 命令

tc 可以使用以下命令对 QDisc、类和过滤器进行操作：

- add 在一个节点里加入一个 QDisc、类或者过滤器。添加时，需要传递一个祖先作为参数，传递参数时既可以使用 ID 也可以直接传递设备的根。如果要建立一个 QDisc 或者过滤器，可以使用句柄(handle)来命名；如果要建立一个类，可以使用类识别符(classid)来命名
- remove 删除有某个句柄(handle)指定的 QDisc，根 QDisc(root)也可以删除。被删除 QDisc 上的所有子类以及附属于各个类的过滤器都会被自动删除

- change 以替代的方式修改某些条目。除了句柄(handle)和祖先不能修改以外，change 命令的语法和 add 命令相同

- replace 对一个现有节点进行近于原子操作的删除／添加。如果节点不存在，这个命令就会建立节点

- link 只适用于 DQisc，替代一个现有的节点

例如:

```shell
tc qdisc [ add | change | replace | link ] dev DEV [ parent qdisc-id | root ] [ handle qdisc-id ] qdisc [ qdisc specific parameters ]

tc class [ add | change | replace ] dev DEV parent qdisc-id [ classid class-id ] qdisc [ qdisc specific parameters ]

tc filter [ add | change | replace ] dev DEV [ parent qdisc-id | root ] protocol protocol prio priority filtertype [ filtertype specific parameters ] flowid flow-id

tc [-s | -d ] qdisc show [ dev DEV ]

tc [-s | -d ] class show dev DEV tc filter show dev DEV
```

## 具体操作

Linux 流量控制主要分为建立队列、建立分类和建立过滤器三个方面，基本实现步骤为：

- 针对网络物理设备（如以太网卡 eth0）绑定一个队列 QDisc

- 在该队列上建立分类 class

- 为每一分类建立一个基于路由的过滤器 filter

- 最后与过滤器相配合，建立特定的路由表

环境模拟实例：

流量控制器上的以太网卡(eth0) 的 IP 地址为 192.168.1.66，在其上建立一个 CBQ 队列。假设包的平均大小为 1000 字节，包间隔发送单元的大小为 8 字节，可接收冲突的发送最长包数目为 20 字节。

假如有三种类型的流量需要控制:

- 发往主机 1 的，其 IP 地址为 192.168.1.24。其流量带宽控制在 8Mbit，优先级为 2
- 发往主机 2 的，其 IP 地址为 192.168.1.30。其流量带宽控制在 1Mbit，优先级为 1
- 发往子网 1 的，其子网号为 192.168.1.0，子网掩码为 255.255.255.0，流量带宽控制在 1Mbit，优先级为 6

cbq 具体操作如下:

- 建立队列

  一般情况下，针对一个网卡只建立一个队列，将一个 cbq 队列绑定到网络物理设备 eth0 上，其编号为 1:0；网络物理设备 eth0 的实际带宽为 10Mbit，包的平均大小为 1000 字节；包间隔发送单元的大小为 8 字节，最小传输包大小为 64 字节 `tc qdisc add dev eth0 root handle 1: cbq bandwidth 10Mbit avpkt 1000 cell 8 mpu 64`

- 建立分类

  分类建立在队列之上,一般情况下，针对一个队列需建立一个根分类，然后再在其上建立子分类。对于分类，按其分类的编号顺序起作用，编号小的优先；一旦符合某个分类匹配规则，通过该分类发送数据包，则其后的分类不再起作用

  - 创建根分类 1:1 分配带宽为 10Mbit，优先级别为 8 `tc class add dev eth0 parent 1:0 classid 1:1 cbq bandwidth 10Mbit rate 10Mbit maxburst 20 allot 1514 prio 8 avpkt 1000 cell 8 weight 1Mbit`

    该队列的最大可用带宽为 10Mbit，实际分配的带宽为 10Mbit，可接收冲突的发送最长包数目为 20 字节；最大传输单元加 MAC 头的大小为 1514 字节，优先级别为 8，包的平均大小为 1000 字节，包间隔发送单元的大小为 8 字节，相应于实际带宽的加权速率为 1Mbit。

  - 创建分类 1:2 其父分类为 1:1，分配带宽为 8Mbit，优先级别为 2 `tc class add dev eth0 parent 1:1 classid 1:2 cbq bandwidth 10Mbit rate 8Mbit maxburst 20 allot 1514 prio 2 avpkt 1000 cell 8 weight 800Kbit split 1:0 bounded`

    该队列的最大可用带宽为 10Mbit，实际分配的带宽为 8Mbit，可接收冲突的发送最长包数目为 20 字节；最大传输单元加 MAC 头的大小为 1514 字节，优先级别为 2，包的平均大小为 1000 字节，包间隔发送单元的大小为 8 字节，相应于实际带宽的加权速率为 800Kbit，分类的分离点为 1:0，且不可借用未使用带宽

  - 创建分类 1:3 其父分类为 1:1，分配带宽为 1Mbit，优先级别为 1 `tc class add dev eth0 parent 1:1 classid 1:3 cbq bandwidth 10Mbit rate 1Mbit maxburst 20 allot 1514 prio 1 avpkt 1000 cell 8 weight 100Kbit split 1:0`

    该队列的最大可用带宽为 10Mbit，实际分配的带宽为 1Mbit，可接收冲突的发送最长包数目为 20 字节；最大传输单元加 MAC 头的大小为 1514 字节，优先级别为 1，包的平均大小为 1000 字节，包间隔发送单元的大小为 8 字节，相应于实际带宽的加权速率为 100Kbit，分类的分离点为 1:0

  - 创建分类 1:4 其父分类为 1:1，分配带宽为 1Mbit，优先级别为 6 `tc class add dev eth0 parent 1:1 classid 1:4 cbq bandwidth 10Mbit rate 1Mbit maxburst 20 allot 1514 prio 6 avpkt 1000 cell 8 weight 100Kbit split 1:0`

    该队列的最大可用带宽为 10Mbit，实际分配的带宽为 1Mbit，可接收冲突的发送最长包数目为 20 字节；最大传输单元加 MAC 头的大小为 1514 字节，优先级别为 6，包的平均大小为 1000 字节，包间隔发送单元的大小为 8 字节，相应于实际带宽的加权速率为 100Kbit，分类的分离点为 1:0

- 建立过滤器

  过滤器主要服务于分类,一般只需针对根分类提供一个过滤器，然后为每个子分类提供路由映射

  - 应用路由过滤器到 cbq 队列的根，父分类编号为 1:0；过滤协议为 ip，优先级别为 100，过滤器为基于路由表 `tc filter add dev eth0 parent 1:0 protocol ip prio 100 route`

  - 建立路由映射分类 1:2, 1:3, 1:4

    ```shell
    tc filter add dev eth0 parent 1:0 protocol ip prio 100 route to 2 flowid 1:2

    tc filter add dev eth0 parent 1:0 protocol ip prio 100 route to 3 flowid 1:3

    tc filter add dev eth0 parent 1:0 protocol ip prio 100 route to 4 flowid 1:4
    ```

- 建立路由

  该路由是与前面所建立的路由映射一一对应

  - 发往主机 192.168.1.24 的数据包通过分类 2 转发(分类 2 的速率 8Mbit) `ip route add 192.168.1.24 dev eth0 via 192.168.1.66 realm 2`

  - 发往主机 192.168.1.30 的数据包通过分类 3 转发(分类 3 的速率 1Mbit) `ip route add 192.168.1.30 dev eth0 via 192.168.1.66 realm 3`

  - 发往子网 192.168.1.0/24 的数据包通过分类 4 转发(分类 4 的速率 1Mbit) `ip route add 192.168.1.0/24 dev eth0 via 192.168.1.66 realm 4`

  PS: 一般对于流量控制器所直接连接的网段建议使用 IP 主机地址流量控制限制，不要使用子网流量控制限制。如一定需要对直连子网使用子网流量控制限制，则在建立该子网的路由映射前，需将原先由系统建立的路由删除，才可完成相应步骤

## 监视

主要包括对现有队列、分类、过滤器和路由的状况进行监视

- 显示队列的状况

  简单显示指定设备(这里为 eth0)的队列状况

  ```shell
  tc qdisc ls dev eth0

  qdisc cbq 1: rate 10Mbit (bounded,isolated) prio no-transmit
  ```

  详细显示指定设备(这里为 eth0)的队列状况

  ```shell
  tc -s qdisc ls dev eth0

  qdisc cbq 1: rate 10Mbit (bounded,isolated) prio no-transmit
  Sent 7646731 bytes 13232 pkts (dropped 0, overlimits 0)  borrowed 0 overactions 0 avgidle 31 undertime 0
  ```

  这里主要显示了通过该队列发送了 13232 个数据包，数据流量为 7646731 个字节，丢弃的包数目为 0，超过速率限制的包数目为 0

- 显示分类的状况

  简单显示指定设备(这里为 eth0)的分类状况

  ```shell
  tc class ls dev eth0

  class cbq 1: root rate 10Mbit (bounded,isolated) prio no-transmit
  class cbq 1:1 parent 1: rate 10Mbit prio no-transmit #no-transmit表示优先级为8
  class cbq 1:2 parent 1:1 rate 8Mbit prio 2
  class cbq 1:3 parent 1:1 rate 1Mbit prio 1
  class cbq 1:4 parent 1:1 rate 1Mbit prio 6
  ```

  详细显示指定设备(这里为 eth0)的分类状况

  ```shell
  tc -s class ls dev eth0

  class cbq 1: root rate 10Mbit (bounded,isolated) prio no-transmit      Sent 17725304 bytes 32088 pkts (dropped 0, overlimits 0)      borrowed 0 overactions 0 avgidle 31 undertime 0

  class cbq 1:1 parent 1: rate 10Mbit prio no-transmit      Sent 16627774 bytes 28884 pkts (dropped 0, overlimits 0)      borrowed 16163 overactions 0 avgidle 587 undertime 0

  class cbq 1:2 parent 1:1 rate 8Mbit prio 2      Sent 628829 bytes 3130 pkts (dropped 0, overlimits 0)      borrowed 0 overactions 0 avgidle 4137 undertime 0

  class cbq 1:3 parent 1:1 rate 1Mbit prio 1      Sent 0 bytes 0 pkts (dropped 0, overlimits 0)      borrowed 0 overactions 0 avgidle 159654 undertime 0

  class cbq 1:4 parent 1:1 rate 1Mbit prio 6      Sent 5552879 bytes 8076 pkts (dropped 0, overlimits 0)      borrowed 3797 overactions 0 avgidle 159557 undertime 0

  ```

  这里主要显示了通过不同分类发送的数据包，数据流量，丢弃的包数目，超过速率限制的包数目等等。其中根分类(class cbq 1:0)的状况应与队列的状况类似。

  例如，分类 class cbq 1:4 发送了 8076 个数据包，数据流量为 5552879 个字节，丢弃的包数目为 0，超过速率限制的包数目为 0。

- 显示过滤器的状况

  ```shell
  tc -s filter ls dev eth0

  filter parent 1: protocol ip pref 100 route
  filter parent 1: protocol ip pref 100 route fh 0xffff0002 flowid 1:2 to 2
  filter parent 1: protocol ip pref 100 route fh 0xffff0003 flowid 1:3 to 3
  filter parent 1: protocol ip pref 100 route fh 0xffff0004 flowid 1:4 to 4
  ```

  这里 flowid 1:2 代表分类 class cbq 1:2，to 2 代表通过路由 2 发送。

- 显示现有路由的状况

  ```shell
  192.168.1.66 dev eth0 scope link
  192.168.1.24 via 192.168.1.66 dev eth0 realm 2
  192.168.1.30 via 192.168.1.66 dev eth0 realm 3
  192.168.1.0/24 via 192.168.1.66 dev eth0 realm 4
  192.168.1.0/24 dev eth0 proto kernel scope link src 192.168.1.66
  default via 192.168.1.254 dev eth0
  ```

  如上所示，结尾包含有 realm 的显示行是起作用的路由过滤器

## 维护

主要包括对队列、分类、过滤器和路由的增添、修改和删除

增添动作一般依照"队列->分类->过滤器->路由"的顺序进行；修改动作则没有什么要求；删除则依照"路由->过滤器->分类->队列"的顺序进行。

- 队列的维护

  一般对于一台流量控制器来说，出厂时针对每个以太网卡均已配置好一个队列了，通常情况下对队列无需进行增添、修改和删除动作

- 分类的维护

  - 增添 增添动作通过 tc class add 命令实现，如前面所示

  - 修改 修改动作通过 tc class change 命令实现，如下所示：`tc class change dev eth0 parent 1:1 classid 1:2 cbq bandwidth 10Mbit rate 7Mbit maxburst 20 allot 1514 prio 2 avpkt 1000 cell 8 weight 700Kbit split 1:0 bounded`

    对于 bounded 命令应慎用，一旦添加后就禁止修改，只可通过删除后再添加来实现

  - 删除 删除动作只在该分类没有工作前才可进行，一旦通过该分类发送过数据，则无法删除它了。因此，需要通过 shell 文件方式来修改，通过重新启动来完成删除动作。

- 过滤器的维护

  - 增添 增添动作通过 tc filter add 命令实现，如前面所示

  - 修改 修改动作通过 tc filter change 命令实现，如下所示: `tc filter change dev eth0 parent 1:0 protocol ip prio 100 route to 10 flowid 1:8`

  - 删除 删除动作通过 tc filter del 命令实现，如下所示: `tc filter del dev eth0 parent 1:0 protocol ip prio 100 route to 10`

- 与过滤器一一映射路由的维护

  - 增添 增添动作通过 ip route add 命令实现，如前面所示

  - 修改 修改动作通过 ip route change 命令实现，如下所示：`ip route change 192.168.1.30 dev eth0 via 192.168.1.66 realm 8`

  - 删除 删除动作通过 ip route del 命令实现，如下所示：

    ```shell
    ip route del 192.168.1.30 dev eth0 via 192.168.1.66 realm 8

    ip route del 192.168.1.0/24 dev eth0 via 192.168.1.66 realm 4
    ```
