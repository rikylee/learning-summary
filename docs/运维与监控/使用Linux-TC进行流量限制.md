# 使用 Linux TC 进行流量限制

## Linux 下的流量控制原理

通过对包的排队，我们可以控制数据包的发送方式。这种控制，称之为数据整形，shape the data，包括对数据的以下操作:

- 增加延时
- 丢包
- 重新排列
- 重复、损坏
- 速率控制

在 qdisc-class-filter 结构下，对流量进行控制需要进行三个步骤:

- 创建 qdisc 队列
  上面提到 Linux 是通过包的排队进行流量的控制，那么首先得有一个队列。

- 创建 class 分类
  class 实际上，就是划分流量策略分类。比如划分两档流量限速 10MBps、20MBbs。

- 创建 filter 过滤
  虽然创建了 class 分类，但是并没有将任何的 IP、Port 绑定到 class 上，此时并不会有控制作用。还需要创建 filter 将指定的 IP、Port 绑定到 class 上，才能使流量控制 class 生效于资源。

TC 是 Linux 下提供的流量控制工具，也是 Cilium/eBPF 等网络组件的核心基础设施之一。

未使用 TC 进行流量控制情况下，使用 iperf3 在`10.3.175.212`向`10.3.175.25`发送数据包，带宽为千兆

```shell
[root@pinyin-server ~]# iperf3 -c 10.3.175.25 -t 10
Connecting to host 10.3.175.25, port 5201
[  4] local 10.3.175.212 port 46094 connected to 10.3.175.25 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   114 MBytes   957 Mbits/sec    0    368 KBytes
[  4]   1.00-2.00   sec   112 MBytes   937 Mbits/sec    0    368 KBytes
[  4]   2.00-3.00   sec   113 MBytes   947 Mbits/sec    0    382 KBytes
[  4]   3.00-4.00   sec   111 MBytes   935 Mbits/sec    0    382 KBytes
[  4]   4.00-5.00   sec   112 MBytes   942 Mbits/sec    0    382 KBytes
[  4]   5.00-6.00   sec   112 MBytes   942 Mbits/sec    0    382 KBytes
[  4]   6.00-7.00   sec   112 MBytes   944 Mbits/sec    0    399 KBytes
[  4]   7.00-8.00   sec   112 MBytes   940 Mbits/sec    0    399 KBytes
[  4]   8.00-9.00   sec   112 MBytes   942 Mbits/sec    0    399 KBytes
[  4]   9.00-10.00  sec   112 MBytes   943 Mbits/sec    0    399 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.10 GBytes   943 Mbits/sec    0             sender
[  4]   0.00-10.00  sec  1.10 GBytes   941 Mbits/sec                  receiver

iperf Done.
```

未使用 TC 进行流量控制情况下，使用 iperf3 在`10.3.175.25`向`10.3.175.212`发送数据包，带宽为千兆

```shell
[root@data01 ~]# iperf3 -c 10.3.175.212 -t 10
Connecting to host 10.3.175.212, port 5201
[  4] local 10.3.175.25 port 57448 connected to 10.3.175.212 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   114 MBytes   954 Mbits/sec    0    444 KBytes
[  4]   1.00-2.00   sec   113 MBytes   946 Mbits/sec    0    522 KBytes
[  4]   2.00-3.00   sec   112 MBytes   941 Mbits/sec    3    379 KBytes
[  4]   3.00-4.00   sec   112 MBytes   943 Mbits/sec    3    247 KBytes
[  4]   4.00-5.00   sec   112 MBytes   942 Mbits/sec    2    334 KBytes
[  4]   5.00-6.00   sec   112 MBytes   939 Mbits/sec    3    253 KBytes
[  4]   6.00-7.00   sec   112 MBytes   940 Mbits/sec    3    222 KBytes
[  4]   7.00-8.00   sec   112 MBytes   940 Mbits/sec    2    338 KBytes
[  4]   8.00-9.00   sec   112 MBytes   940 Mbits/sec    2    301 KBytes
[  4]   9.00-10.00  sec   112 MBytes   941 Mbits/sec    4    204 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.10 GBytes   943 Mbits/sec   22             sender
[  4]   0.00-10.00  sec  1.10 GBytes   941 Mbits/sec                  receiver

iperf Done.
```

## 限制指定 IP、Port 对本机的访问速度

### 查看网卡

```shell
[root@pinyin-server ~]# ifconfig

ens192: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.3.175.212  netmask 255.255.252.0  broadcast 10.3.175.255
        inet6 fe80::5951:e3af:825d:7fb9  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:da:c9:e7  txqueuelen 1000  (Ethernet)
        RX packets 373135833  bytes 58781065933 (54.7 GiB)
        RX errors 0  dropped 3248889  overruns 0  frame 0
        TX packets 282980546  bytes 109561222466 (102.0 GiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

### 配置设备`ens192` qdisc-class-filter

- 创建 qdisc 根队列

```shell
[root@pinyin-server ~]# tc qdisc show dev ens192
qdisc mq 0: root
qdisc pfifo_fast 0: parent :4 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :3 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :1 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
[root@pinyin-server ~]# tc qdisc add dev ens192 root handle 1: htb default 1
[root@pinyin-server ~]# tc qdisc show dev ens192
qdisc htb 1: root refcnt 5 r2q 10 default 1 direct_packets_stat 21 direct_qlen 1000
```

- 创建第一级 class 绑定所有带宽资源,例如我这边带宽上限为千兆

```shell
[root@pinyin-server ~]# tc class show dev ens192
[root@pinyin-server ~]# tc class add dev ens192 parent 1:0 classid 1:1 htb rate 1000mbit
[root@pinyin-server ~]# tc class show dev ens192
class htb 1:1 root prio 0 rate 1Gbit ceil 1Gbit burst 1375b cburst 1375b
```

- 创建子分类 class，限制带宽上限 10Mbits/sec，网络空闲的情况下带宽上限 20Mbits/sec，允许超过上/下限 15Kb/s 的带宽

```shell
[root@pinyin-server ~]# tc class add dev ens192 parent 1:1 classid 1:10 htb rate 10mbit ceil 20mbit burst 15k
[root@pinyin-server ~]# tc class show dev ens192
class htb 1:10 parent 1:1 prio 0 rate 10Mbit ceil 20Mbit burst 15Kb cburst 1600b
class htb 1:1 root rate 1Gbit ceil 1Gbit burst 1375b cburst 1375b
```

- 创建过滤器 filter，限制 IP,prio 越小优先级高

```shell
[root@pinyin-server ~]# tc filter show dev ens192
[root@pinyin-server ~]# tc filter add dev ens192 protocol ip parent 1:0 prio 1 u32 match ip dst 10.3.175.25 flowid 1:10
[root@pinyin-server ~]# tc filter show dev ens192
filter parent 1: protocol ip pref 1 u32
filter parent 1: protocol ip pref 1 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 1 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 1:10 not_in_hw
  match 0a03af19/ffffffff at 16

```

- 查看配置是否生效

使用 iperf3 在`10.3.175.212`向`10.3.175.25`发送数据包，带宽在 18.8 Mbits/sec~24.8 Mbits/sec 之间，说明配置生效了，`10.3.175.212`向`10.3.175.25`的出口带宽限制在 20Mbits/sec

```shell
[root@pinyin-server ~]# iperf3 -c 10.3.175.25 -t 10
Connecting to host 10.3.175.25, port 5201
[  4] local 10.3.175.212 port 46098 connected to 10.3.175.25 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec  2.95 MBytes  24.8 Mbits/sec    0    161 KBytes
[  4]   1.00-2.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   2.00-3.00   sec  2.61 MBytes  21.9 Mbits/sec    0    161 KBytes
[  4]   3.00-4.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   4.00-5.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   5.00-6.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   6.00-7.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   7.00-8.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   8.00-9.00   sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
[  4]   9.00-10.00  sec  2.24 MBytes  18.8 Mbits/sec    0    161 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  23.5 MBytes  19.7 Mbits/sec    0             sender
[  4]   0.00-10.00  sec  22.8 MBytes  19.2 Mbits/sec                  receiver

iperf Done.

```

使用 iperf3 在`10.3.175.25`向`10.3.175.212`发送数据包，带宽仍然为千兆

```shell
[root@data01 ~]# iperf3 -c 10.3.175.212 -t 10
Connecting to host 10.3.175.212, port 5201
[  4] local 10.3.175.25 port 34750 connected to 10.3.175.212 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   114 MBytes   955 Mbits/sec    0    452 KBytes
[  4]   1.00-2.00   sec   112 MBytes   940 Mbits/sec    0    510 KBytes
[  4]   2.00-3.00   sec   113 MBytes   946 Mbits/sec    3    362 KBytes
[  4]   3.00-4.00   sec   112 MBytes   938 Mbits/sec    3    294 KBytes
[  4]   4.00-5.00   sec   112 MBytes   941 Mbits/sec    3    290 KBytes
[  4]   5.00-6.00   sec   112 MBytes   944 Mbits/sec    4    276 KBytes
[  4]   6.00-7.00   sec   112 MBytes   939 Mbits/sec    1    354 KBytes
[  4]   7.00-8.00   sec   112 MBytes   940 Mbits/sec    3    320 KBytes
[  4]   8.00-9.00   sec   113 MBytes   947 Mbits/sec    2    358 KBytes
[  4]   9.00-10.00  sec   112 MBytes   938 Mbits/sec    2    397 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.10 GBytes   943 Mbits/sec   21             sender
[  4]   0.00-10.00  sec  1.09 GBytes   940 Mbits/sec                  receiver

iperf Done.

```

- 清理全部配置

清理配置之后，使用 iperf3 在`10.3.175.212`向`10.3.175.25`发送数据包，带宽已经恢复千兆

```shell
[root@pinyin-server ~]# tc qdisc del dev ens192 root
[root@pinyin-server ~]# tc qdisc show dev ens192
qdisc mq 0: root
qdisc pfifo_fast 0: parent :4 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :3 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :1 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
[root@pinyin-server ~]# tc class show dev ens192
class mq :1 root
class mq :2 root
class mq :3 root
class mq :4 root
[root@pinyin-server ~]# tc filter show dev ens192
[root@pinyin-server ~]# iperf3 -c 10.3.175.25 -t 10
Connecting to host 10.3.175.25, port 5201
[  4] local 10.3.175.212 port 46102 connected to 10.3.175.25 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   114 MBytes   956 Mbits/sec    0    375 KBytes
[  4]   1.00-2.00   sec   112 MBytes   941 Mbits/sec    0    387 KBytes
[  4]   2.00-3.00   sec   112 MBytes   936 Mbits/sec    0    387 KBytes
[  4]   3.00-4.00   sec   112 MBytes   942 Mbits/sec    0    387 KBytes
[  4]   4.00-5.00   sec   113 MBytes   947 Mbits/sec    0    416 KBytes
[  4]   5.00-6.00   sec   112 MBytes   940 Mbits/sec    0    416 KBytes
[  4]   6.00-7.00   sec   112 MBytes   940 Mbits/sec    0    416 KBytes
[  4]   7.00-8.00   sec   112 MBytes   941 Mbits/sec    0    431 KBytes
[  4]   8.00-9.00   sec   112 MBytes   940 Mbits/sec    0    431 KBytes
[  4]   9.00-10.00  sec   112 MBytes   941 Mbits/sec    0    431 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.10 GBytes   943 Mbits/sec    0             sender
[  4]   0.00-10.00  sec  1.10 GBytes   941 Mbits/sec                  receiver

iperf Done.
```

## 限制本机对指定 IP、Port 的访问速度

由于排队规则主要是基于出口方向，不能对入口方向的流量（Ingress）进行限制。因此，我们需要将流量重定向到 ifb 设备上，再对 ifb 的出口流量（Egress）进行限制，以最终达到控制的目的

### 启用虚拟网卡

- 加载 ifb 设备

```shell
[root@pinyin-server ~]# modprobe ifb numifbs=1
[root@pinyin-server ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
      valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
      valid_lft forever preferred_lft forever
2: ens192: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0c:29:da:c9:e7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.175.212/22 brd 10.3.175.255 scope global noprefixroute ens192
      valid_lft forever preferred_lft forever
    inet6 fe80::5951:e3af:825d:7fb9/64 scope link noprefixroute
      valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:49:a8:80:b0 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
      valid_lft forever preferred_lft forever
    inet6 fe80::42:49ff:fea8:80b0/64 scope link
      valid_lft forever preferred_lft forever
100: ifb0: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default qlen 32
    link/ether b6:f9:e1:95:a6:33 brd ff:ff:ff:ff:ff:ff
```

- 启用 ifb0 虚拟设备

```shell
[root@pinyin-server ~]# ip link set dev ifb0 up
[root@pinyin-server ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
      valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
      valid_lft forever preferred_lft forever
2: ens192: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0c:29:da:c9:e7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.175.212/22 brd 10.3.175.255 scope global noprefixroute ens192
      valid_lft forever preferred_lft forever
    inet6 fe80::5951:e3af:825d:7fb9/64 scope link noprefixroute
      valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:49:a8:80:b0 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
      valid_lft forever preferred_lft forever
    inet6 fe80::42:49ff:fea8:80b0/64 scope link
      valid_lft forever preferred_lft forever
100: ifb0: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 32
    link/ether b6:f9:e1:95:a6:33 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::b4f9:e1ff:fe95:a633/64 scope link
      valid_lft forever preferred_lft forever
```

### 配置设备`ifb0` qdisc-class-filter

- 添加 qdisc

```shell
[root@pinyin-server ~]# tc qdisc show dev ens192
qdisc mq 0: root
qdisc pfifo_fast 0: parent :4 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :3 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :1 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
[root@pinyin-server ~]# tc qdisc add dev ens192 handle ffff: ingress
[root@pinyin-server ~]# tc qdisc show dev ens192
qdisc mq 0: root
qdisc pfifo_fast 0: parent :4 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :3 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :1 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc ingress ffff: parent ffff:fff1 ----------------

```

- 重定向网卡流量到 ifb0

```shell
[root@pinyin-server ~]# tc filter add dev ens192 parent ffff: protocol ip u32 match u32 0 0 action mirred egress redirect dev ifb0
```

- 添加 class 和 filter

```shell
tc qdisc add dev ifb0 root handle 1: htb default 10
tc class add dev ifb0 parent 1:0 classid 1:1 htb rate 1000mbit
tc class add dev ifb0 parent 1:1 classid 1:10 htb rate 5mbit
tc filter add dev ifb0 parent 1:0 protocol ip prio 16 u32 match ip dst 10.3.172.25 flowid 1:10

[root@pinyin-server ~]# tc qdisc add dev ifb0 root handle 1: htb default 10
[root@pinyin-server ~]# tc qdisc show dev ifb0
qdisc htb 1: root refcnt 2 r2q 10 default 10 direct_packets_stat 303 direct_qlen 32
[root@pinyin-server ~]# tc class add dev ifb0 parent 1:0 classid 1:1 htb rate 1000mbit
[root@pinyin-server ~]# tc class show dev ifb0
class htb 1:1 root prio 0 rate 1Gbit ceil 1Gbit burst 1375b cburst 1375b
[root@pinyin-server ~]# tc class add dev ifb0 parent 1:1 classid 1:10 htb rate 5mbit
[root@pinyin-server ~]# tc class show dev ifb0
class htb 1:10 parent 1:1 prio 0 rate 5Mbit ceil 5Mbit burst 1600b cburst 1600b
class htb 1:1 root rate 1Gbit ceil 1Gbit burst 1375b cburst 1375b
[root@pinyin-server ~]# tc filter show dev ifb0
[root@pinyin-server ~]# tc filter add dev ifb0 parent 1:0 protocol ip prio 16 u32 match ip dst 10.3.172.25 flowid 1:10
[root@pinyin-server ~]# tc filter show dev ifb0
filter parent 1: protocol ip pref 16 u32
filter parent 1: protocol ip pref 16 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 16 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 1:10 not_in_hw
  match 0a03ac19/ffffffff at 16

```

- 查看配置是否生效

使用 iperf3 在`10.3.175.212`向`10.3.175.25`发送数据包，带宽在千兆

```shell
[root@pinyin-server ~]# iperf3 -c 10.3.175.25 -t 10
Connecting to host 10.3.175.25, port 5201
[  4] local 10.3.175.212 port 46106 connected to 10.3.175.25 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   109 MBytes   913 Mbits/sec    0    779 KBytes
[  4]   1.00-2.00   sec   112 MBytes   944 Mbits/sec    0    803 KBytes
[  4]   2.00-3.00   sec   112 MBytes   944 Mbits/sec    0    803 KBytes
[  4]   3.00-4.00   sec   112 MBytes   943 Mbits/sec    0    829 KBytes
[  4]   4.00-5.00   sec   111 MBytes   933 Mbits/sec    0    829 KBytes
[  4]   5.00-6.00   sec   112 MBytes   944 Mbits/sec    0    829 KBytes
[  4]   6.00-7.00   sec   112 MBytes   944 Mbits/sec    0    829 KBytes
[  4]   7.00-8.00   sec   111 MBytes   933 Mbits/sec    0    829 KBytes
[  4]   8.00-9.00   sec   112 MBytes   944 Mbits/sec    0    829 KBytes
[  4]   9.00-10.00  sec   112 MBytes   944 Mbits/sec    0    829 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.09 GBytes   939 Mbits/sec    0             sender
[  4]   0.00-10.00  sec  1.09 GBytes   936 Mbits/sec                  receiver

iperf Done.

```

使用 iperf3 在`10.3.175.25`向`10.3.175.212`发送数据包，带宽在 3.65 Mbits/sec~11.0 Mbits/sec 之间，说明配置生效了，`10.3.175.25`向`10.3.175.212`的入口带宽虽然有浮动，但仍然限制在 5Mbits/sec

```shell
[root@data01 ~]# iperf3 -c 10.3.175.212 -t 10
Connecting to host 10.3.175.212, port 5201
[  4] local 10.3.175.25 port 47132 connected to 10.3.175.212 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec  1.31 MBytes  11.0 Mbits/sec   50    154 KBytes
[  4]   1.00-2.00   sec  1018 KBytes  8.35 Mbits/sec  104    109 KBytes
[  4]   2.00-3.00   sec   445 KBytes  3.65 Mbits/sec    0    123 KBytes
[  4]   3.00-4.00   sec   573 KBytes  4.69 Mbits/sec    4   82.0 KBytes
[  4]   4.00-5.00   sec   445 KBytes  3.65 Mbits/sec   15   52.3 KBytes
[  4]   5.00-6.00   sec   891 KBytes  7.30 Mbits/sec    0   59.4 KBytes
[  4]   6.00-7.00   sec   445 KBytes  3.65 Mbits/sec    0   66.5 KBytes
[  4]   7.00-8.00   sec   445 KBytes  3.65 Mbits/sec    0   73.5 KBytes
[  4]   8.00-9.00   sec   891 KBytes  7.30 Mbits/sec    0   79.2 KBytes
[  4]   9.00-10.00  sec   445 KBytes  3.65 Mbits/sec    0   83.4 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  6.78 MBytes  5.69 Mbits/sec  173             sender
[  4]   0.00-10.00  sec  5.73 MBytes  4.81 Mbits/sec                  receiver

iperf Done.
```

- 清理全部配置

```
[root@pinyin-server ~]# tc qdisc del dev ens192 ingress
[root@pinyin-server ~]# tc qdisc del dev ifb0 root
[root@pinyin-server ~]#
[root@pinyin-server ~]# tc qdisc show dev ens192
qdisc mq 0: root
qdisc pfifo_fast 0: parent :4 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :3 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
qdisc pfifo_fast 0: parent :1 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
[root@pinyin-server ~]#
[root@pinyin-server ~]# tc qdisc show dev ifb0
qdisc pfifo_fast 0: root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
[root@pinyin-server ~]# tc class show dev ifb0
[root@pinyin-server ~]# tc filter show dev ifb0
[root@pinyin-server ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens192: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0c:29:da:c9:e7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.175.212/22 brd 10.3.175.255 scope global noprefixroute ens192
       valid_lft forever preferred_lft forever
    inet6 fe80::5951:e3af:825d:7fb9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:49:a8:80:b0 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:49ff:fea8:80b0/64 scope link
       valid_lft forever preferred_lft forever
100: ifb0: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 32
    link/ether b6:f9:e1:95:a6:33 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::b4f9:e1ff:fe95:a633/64 scope link
       valid_lft forever preferred_lft forever
[root@pinyin-server ~]#
[root@pinyin-server ~]# modprobe -r ifb
[root@pinyin-server ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens192: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0c:29:da:c9:e7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.175.212/22 brd 10.3.175.255 scope global noprefixroute ens192
       valid_lft forever preferred_lft forever
    inet6 fe80::5951:e3af:825d:7fb9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:49:a8:80:b0 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:49ff:fea8:80b0/64 scope link
       valid_lft forever preferred_lft forever
```

重新测试`10.3.172.25`向`10.3.175.212` 发送数据包，带宽已经恢复千兆

```
[root@data01 ~]# iperf3 -c 10.3.175.212 -t 10
Connecting to host 10.3.175.212, port 5201
[  4] local 10.3.175.25 port 48906 connected to 10.3.175.212 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   114 MBytes   954 Mbits/sec    0    468 KBytes
[  4]   1.00-2.00   sec   112 MBytes   942 Mbits/sec    3    226 KBytes
[  4]   2.00-3.00   sec   112 MBytes   938 Mbits/sec    0    477 KBytes
[  4]   3.00-4.00   sec   112 MBytes   942 Mbits/sec    3    358 KBytes
[  4]   4.00-5.00   sec   112 MBytes   939 Mbits/sec    2    450 KBytes
[  4]   5.00-6.00   sec   112 MBytes   942 Mbits/sec    3    260 KBytes
[  4]   6.00-7.00   sec   113 MBytes   945 Mbits/sec    1    341 KBytes
[  4]   7.00-8.00   sec   112 MBytes   939 Mbits/sec    2    430 KBytes
[  4]   8.00-9.00   sec   112 MBytes   939 Mbits/sec    3    373 KBytes
[  4]   9.00-10.00  sec   113 MBytes   947 Mbits/sec    3    301 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.10 GBytes   943 Mbits/sec   20             sender
[  4]   0.00-10.00  sec  1.09 GBytes   941 Mbits/sec                  receiver

iperf Done.
```
