# 监控概述

## 监控目的

我们应建立完善的监控体系，以期达到如下效果

- 趋势分析：长期收集并统计监控样本数据，对监控指标进行趋势分析。例如，通过分析磁盘的使用空间增长率，可以预测何时需要对磁盘进行扩容。
- 对照分析：随时掌握系统的不同版本在运行时资源使用情况的差异，或在不同容量的环境下系统并发和负载的区别。
- 告警：当系统即将出现故障或已经出现故障时，监控可以迅速反应并发出告警。这样，管理员就可以提前预防问题发生或快速处理已产生的问题，从而保证业务服务的正常运行。
- 故障分析与定位：故障发生时，技术人员需要对故障进行调查和处理。通过分析监控系统记录的各种历史数据，可以迅速找到问题的根源并解决问题。
- 数据可视化：通过监控系统获取的数据，可以生成可视化仪表盘，使运维人员能够直观地了解系统运行状态、资源使用情况、服务运行状态等。

## 监控维度

通常情况下，监控系统分为端监控、业务层监控、应用层监控、中间件监控、系统层监控这 5 层

- 端监控：针对用户在体验上可以感知的对象进行监控，如网站、App、小程序等。有些公司会设置专门的端用户体验团队负责进行端监控。在移动客户端的系统中，端监控的对象主要有 H5、小程序、Android 系统、iOS 系统等，完善的端监控可以反馈地域、渠道、链接等多维度的用户体验信息； 用户终端为传统的 Web 页面时，端监控仍会围绕用户体验采集数据，比如页面打开速度（测速）、页面稳定性（JS）和外部服务调用成功率（API）， 这 3 个方面的数据反映了 Web 页面的健康度。

- 业务层监控：对于业务层，可按需深度定制监控系统，实现对业务属性的监控告警功能，生成业务数据监控大盘。比如用户访问 QPS、DAU 日活、转化率、业务接口（如登录数、注册数、订单量、支付量、搜索量）等都是常见的监控对象

- 应用层监控：主要是对分布式应用和调用链应用的性能进行管理和监控，如对 Spring Boot、JVM 信息、服务链路、Dubbo 等应用在进行诸如 RPC 调用、Trace 链路追踪动作时产生的数据进行监控

- 中间件监控：监控的主要对象是框架自身的埋点、延迟、错误率等。这里的中间件包括但不限于消息中间件（RabbitMQ、Kafka、RocketMQ 等）、数据库中间件（MySQL、Oracle、PostgreSQL、TIDB、PolarDB 等）、数据库连接池中间件（HikariCP、Druid、BoneCP 等）、缓存中间件（Redis、Memcached 等）和 Web 服务中间件（Tomcat、Jetty 等 ）

- 系统层监控：如何对系统层进行监控，是运维工程师最关心的问题。小米通过 Open-Falcon 提炼出了 Linux 系统的运维基础采集项，主要包含 CPU、Load、内存、磁盘 I/O、网络相关参数、内核参数、ss 统计输出、端口、核心服务的进程存活情况、关键业务进程资源消耗、NTP offset 采集、DNS 解析采集等指标。这些都可以作为对系统层监控的关键指标。另外，网络监控也是系统监控中很重要的部分，对交换机、路由器、防火墙、VPN 进行的监控都属于网络监控的范畴，内网和外网的丢包率、网络延迟等也都是很重要的监控指标

## 现有方案

市面上的监控系统可以说是五花八门

- Apache 的 SkyWalking
- 百度的 DP
- 美团的 CAT
- 蚂蚁金服的九色鹿
- 宜信的 UAVstack
- 滴滴的 Omega
- 360 和头条的 Sentry
- 腾讯的 badjs
- 阿里云的 arms
- 商业化的 Fundbug、听云和神策等

通常来说，Zabbix 是针对系统层的监控系统，ELK（Elasticsearch + Logstash + Kibana）主要是做日志监控的，而 Prometheus 和 Grafana 可以实现对端、业务层、应用层、中间件、系统层进行监控，因此 Prometheus 是打造一站式通用监控架构的最佳方案之一

监控系统中的监控功能可以告诉我们系统的哪些部分正常工作，哪里出现了问题；监控系统具有的可观察性可以帮助我们判断出有问题的地方为何不能工作了。除了监控功能和可观察性外，数据分析对监控系统来说也非常重要。云原生计算基金会在其 Landscape 中将可观察性和数据分析单独列为一个分类—Observability and Analysis，这个分类主要包括 Monitoring、Logging、Tracing、Chaos Engineering 这 4 个子类

- Monitoring 子类中的产品与监控相关，包括 Prometheus、Grafana、Zabbix、Nagios 等常见的监控软件，以及 Prometheus 的伴侣 Thanos

- Logging 子类中的产品与日志相关，比如 Elastic、logstash、ﬂuentd、Loki 等开源软件

- Tracing 子类中的产品与追踪相关，包括 Jaeger、SkyWalking、Pinpoint、Zipkin、Spring Cloud Sleuth 等

- Chaos Engineering 是一个新兴的领域。随着云原生系统的演进，系统的稳定性受到很大的挑战，混沌工程通过反脆弱思想，在系统中模拟常见的故障场景，以期提前发现问题。Chaos Engineering 可以帮助分布式系统提升可恢复性和容错性

## 监控架构分类

近年来，随着以 Kubernetes 为代表的云原生技术的崛起，软件的研发流程已经逐步进化到 IaaS 层、Kubernetes 层、团队组织层。

Kubernetes 是强大的声明式容器编排工具，可以提供计算、存储、网络等功能接口，通过这些接口以插件形式实现相关功能。这种灵活、开放的设计理念使 Kubernetes 非常容易集成外部工具，强化相应的功能。所以 Kubernetes 逐渐发展成中间件和微服务的“底座”，同时也成为企业上云的“底座”。Kubernetes 已经可以和诸如 OpenStack、AWS、Google 云等 IaaS 云平台进行集成，在弹性、敏捷、动态方面，它都可以发挥巨大作用。在 IaaS 层可以实现对硬件、网络等的监控；在 Kubernetes 层则可以实现对日志、健康检查、自愈系统、分布式链路等的监控，Kubernetes 层作为中间件和微服务的“底座”，很多产品的监控都可以在这一层完成。

一般来说，开源监控系统由集中式日志解决方案（以 ELK 为代表）和时序数据库解决方案构成。时序数据库解决方案以 Graphite、TICK 和 Prometheus 等为代表，其中前两个是推模式，后一个则以拉模式为主，拉模式对整体代码和架构的侵入较小。

当代新的监控三要素为 Metrics、Logging 和 Tracing

- Metrics 的特点是可聚合（Aggregatable），它是根据时间发生的可以聚合的数据点。通俗地讲，Metrics 是随着时间的推移产生的一些与监控相关的可聚合的重要指标（如与 Counter 计数器、Historgram 等相关的指标）
- Logging 是一种离散日志（或称事件），分为有结构的日志和无结构的日志两种
- Tracing 是一种为请求域内的调用链提供的监控理念

Prometheus 同时覆盖了 Logging 和 Metrics 两个要素，关于 Metrics、Logging、Tracing 的比较如图所示，其中 CapEx 代表搭建的投入成本，OpEx 代表运维成本，Reaction 代表监控手段的响应能力，Investigation 代表查问题的有效程度。一般来说，Metrics 和 HealthCheck 对于监控告警非常有帮助，Metrics、Tracing 和 Logging 对于调试、发现问题非常有帮助

|               | Metrics | Logging | Tracing |
| :------------ | :-----: | :-----: | :-----: |
| CapEx         |   中    |   低    |   高    |
| OpEx          |   低    |   中    |   高    |
| Reaction      |   高    |   中    |   低    |
| Investigation |   低    |   中    |   高    |

Prometheus 是基于 Metrics 的监控系统，具有投入成本（CapEx）中等、运维成本（OpEx） 低、响应能力（Reaction）高等特点

成熟的分布式软件系统在使用过程中可以分为监控告警、问题排查和稳定性保障这 3 个部分，进行监控告警时，HealthCheck○ 一 是运维团队监测应用系统是否存活、是否健康的最后一道防线，这是必须引起重视的一道防线。HealthCheck 在微服务中通过对一个特定的 HTTP 请求进行轮询实现监控。通过对这个请求进行轮询，不但可以得到微服务的监控状态，还可以得到相关中间件如 MQ、Redis、MySQL、配置中心等的健康状态

## 基于 Prometheus 的监控系统

依赖于 prometheus(汇总 metrices) + grafana (dashboard) + prometheus alertmanager (告警)

## 系统层监控

- [node_exporter](https://github.com/prometheus/node_exporter) 采集 RHEL/CentOS/Fedora 服务器指标

  监控范围:服务器 CPU、内存、硬盘、网络等

## 中间件监控

- [mysqld_exporter](https://github.com/prometheus/mysqld_exporter) 采集 mysql 性能指标

  监控范围: mysql 服务状态、读写、耗时、数据库、事件、内存占用等

- [mongodb_exporter](https://github.com/percona/mongodb_exporter) 采集 mongodb 性能指标

  监控范围:

  ```
  MongoDB Server Status metrics (cursors, operations, indexes, storage, etc)
  MongoDB Replica Set metrics (members, ping, replication lag, etc)
  MongoDB Replication Oplog metrics (size, length in time, etc)
  MongoDB Sharding metrics (shards, chunks, db/collections, balancer operations)
  MongoDB RocksDB storage-engine metrics (levels, compactions, cache usage, i/o rates, etc)
  MongoDB WiredTiger storage-engine metrics (cache, blockmanger, tickets, etc)
  MongoDB Top Metrics per collection (writeLock, readLock, query, etc*)
  ```

- [redis_exporter](https://github.com/oliver006/redis_exporter) 采集 redis 性能指标

  监控范围:

  ```
  server: General information about the Redis server
  clients: Client connections section
  memory: Memory consumption related information
  persistence: RDB and AOF related information
  stats: General statistics
  replication: Master/replica replication information
  cpu: CPU consumption statistics
  commandstats: Redis command statistics
  cluster: Redis Cluster section
  modules: Modules section
  keyspace: Database related statistics
  modules: Module related sections
  ```

- [fastdfs-exporter](https://github.com/whithen/fastdfs-exporter) 采集 fastdfs 性能指标

  监控范围: 服务状态、节点状态、硬盘容量、读写等

- [rocketmq_exporer](https://github.com/apache/rocketmq-exporter) 采集 rocketmq 性能指标

  监控范围: broker tps/qps、producer tps/message/offset、consumer tps/message/offset/lantency..

## 业务层监控

## 应用层监控

- k8s 探针 healthCheck 监测服务存活

- 服务调用链路追踪告警

  - istio 全套方案,学习成本高，k8s 原生支持，告警依赖于其他组件、功能很全面，链路只是其一个功能点

  - skywalking AMP 全套方案，学习成本相对低、存在跨平台问题目前支持 java、nodejs、.net core、python 等 agent 直接集成即可无需修改任何代码，php、Go 和 C++ 以 SDK 形式提供，需要自己实现，支持链路异常告警

## 端监控

## 日志监控告警

fluent-bit 采集

定向采集- fluent-bit 内嵌到指定镜像内，只采集该容器日志
全量采集- fluent-bit daemon set 形式每台服务器一个，采集所有日志，在写入 elasticsearch 前过滤
