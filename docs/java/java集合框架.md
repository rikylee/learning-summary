# Java 集合

Java 集合主要是由两大接口派生而来：一个是 Collection 接口，主要用于存放单一元素；另一个是 Map 接口，主要用于存放键值对。 Collection 接口，下面又有三个主要的子接口：List、Set 和 Queue。具体如下：

![Java 集合](https://gitee.com/rikylee/images/raw/master/image/202201101045723.png)

- List, Set, Queue, Map

```txt
List: 存储的元素按照插入顺序、可重复
Set: 存储的元素无序、不可重复
Queue: 存储的元素是有序的、可重复的
Map: 使用键值对（key-value）存储，key 是无序、不可重复的，value 是无序的、可重复的，每个键最多映射到一个值
```

- List 的实现 ArrayList，Vector，LinkedList

```txt
ArrayList：底层使用数组存储，适用于频繁的查找工作，线程不安全， 支持高效的随机元素访问
Vector：底层使用数组存储，内部操作方法使用synchronized关键字，线程安全的
LinkedList：底层使用的是双向链表，适用于频繁的插入和删除元素的操作，线程不安全， 不支持高效的随机元素访问
```

- ArrayList 扩容

```java
//JDK 11的grow方法
private void add(E e, Object[] elementData, int s) {
  if (s == elementData.length)
    elementData = grow();
  elementData[s] = e;
  size = s + 1;
}

private Object[] grow() {
  return grow(size + 1);
}

private Object[] grow(int minCapacity) {
  return elementData = Arrays.copyOf(elementData,newCapacity(minCapacity));
}

/**
*返回新容量
*/
private int newCapacity(int minCapacity) {
  // oldCapacity为旧容量，newCapacity为新容量
  int oldCapacity = elementData.length;
  // 新容量为旧容量的1.5倍
  int newCapacity = oldCapacity + (oldCapacity >> 1);

  //然后检查新容量是否大于最小需要容量

  //若还是小于最小需要容量，如果数组为空，则取DEFAULT_CAPACITY(10)和最小容量中大的最为新容量，如果minCapacity 小于零，抛出OOM异常，否则返回最小需要容量为新容量

  if (newCapacity - minCapacity <= 0) {
      if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA)
          return Math.max(DEFAULT_CAPACITY, minCapacity);
      if (minCapacity < 0) // overflow
          throw new OutOfMemoryError();
      return minCapacity;
  }

  //若是大于最小需要容量，如果新容量小于最大MAX_ARRAY_SIZE(Integer.MAX_VALUE - 8)容量，返回新容量，反正采用超大容量
  return (newCapacity - MAX_ARRAY_SIZE <= 0)
      ? newCapacity
      : hugeCapacity(minCapacity);
}

/**
* 最小容量小于零，OOM
* minCapacity > MAX_ARRAY_SIZE 取 Integer.MAX_VALUE，否则取 MAX_ARRAY_SIZE
*/
private static int hugeCapacity(int minCapacity) {
    if (minCapacity < 0) // overflow
        throw new OutOfMemoryError();
    return (minCapacity > MAX_ARRAY_SIZE)
        ? Integer.MAX_VALUE
        : MAX_ARRAY_SIZE;
}
```

- Set 的实现 HashSet、LinkedHashSet、TreeSet

```txt
HashSet、LinkedHashSet和TreeSet都是Set接口的实现类，都能保证元素唯一，并且都不是线程安全的

HashSet: 基于 HashMap 实现的，底层采用 HashMap 来保存元素，无序
LinkedHashSet: LinkedHashSet是HashSet的子类，并且其内部是通过LinkedHashMap来实现的，保证元素的插入和取出顺序满足FIFO的场景
TreeSet: 红黑树,有序、支持自定义排序（可以指定排序的比较器）
```

- Map 的实现 HashMap、LinkedHashMap、Hashtable、TreeMap

```txt
HashMap： JDK1.8 之前 HashMap 由数组+链表组成的，数组是 HashMap 的主体，链表则是主要为了解决哈希冲突而存在的（“拉链法”解决冲突）。JDK1.8 以后，当链表长度大于阈值（默认为 8）（链表转换成红黑树前会判断，如果当前数组的长度小于 64，那么会选择先进行数组扩容，而不是转换为红黑树）时，将链表转化为红黑树，以减少搜索时间。默认的初始化大小为 16。之后每次扩充，容量变为原来的 2 倍

LinkedHashMap：LinkedHashMap 继承自 HashMap，所以它的底层仍然是基于拉链式散列结构即由数组和链表或红黑树组成。另外，LinkedHashMap 在上面结构的基础上，增加了一条双向链表，使得上面的结构可以保持键值对的插入顺序。同时通过对链表进行相应的操作，实现了访问顺序相关逻辑。

Hashtable：数组+链表组成的，数组是 Hashtable 的主体，链表则是主要为了解决哈希冲突而存在的，是线程安全的，方法使用synchronized修饰，基本被淘汰，不要在代码中使用它。

TreeMap：使用红黑树实现，红黑树,有序、支持自定义排序（可以指定排序的比较器）
```

- ConcurrentHashMap 线程安全的具体实现方式

```txt
底层数据结构： JDK1.7 的 ConcurrentHashMap 底层采用 分段的数组+链表 实现，JDK1.8 采用的数据结构跟 HashMap 1.8 的结构一样，数组+链表/红黑二叉树

实现线程安全的方式：JDK1.7 的时候，ConcurrentHashMap使用分段锁对整个桶数组进行了分割分段(Segment)，每一把锁只锁容器其中一部分数据，多线程访问容器里不同数据段的数据，不会存在锁竞争，提高并发访问率。 JDK1.8 的时候已经摒弃了 Segment 的概念，而是直接用 Node数组+链表+红黑树的数据结构来实现，并发控制使用 synchronized 和 CAS 来操作，synchronized 只锁定当前链表或红黑二叉树的首节点，这样只要 hash 不冲突，就不会产生并发，效率又提升 N 倍


```
