# Java 基础

- String & StringBuilder & StringBuffer 区别，哪个性能更高，哪个线程安全

```txt
String:不可变字符串
StringBuffer：可变字符串、效率低、线程安全,大量方法被synchronized修饰
StringBuilder：可变字符序列、效率高、线程不安全
```

- == 和 equals() 的区别

```txt
对于基本类型来说，== 比较的是值是否相等； 对于引用类型来说，== 比较的是两个引用是否指向同一个对象地址；

equals() 作用不能用于判断基本数据类型的变量，只能用来判断两个对象是否相等。equals 如果没有被重写，对比它们的地址是否相等；如果 equals()方法被重写（例如 String），则比较的是地址里的内容
```
