# 常用的 JVM 参数

## 内存相关

通过这些参数可以对 JVM 的内存分配做调整

- Xms

  英文解释：Initial heap size(in bytes)

  中文释义：堆区初始值

  使用方法：-Xms2g 或 -XX:InitialHeapSize=2048m

- Xmx

  英文解释：Maximum heap size(in bytes)

  中文释义：堆区最大值

  使用方法：-Xmx2g 或 -XX:MaxHeapSize=2048m

- Xmn

  英文解释：Maximum new generation size(in bytes)

  中文释义：新生代最大值，默认情况下，YG 的最小为 1310 MB，最大为无限制

  使用方法：-Xmn512m 或 -XX:NewSize=512m -XX:MaxNewSize=512m

- PermSize：(JDK1.8 以后已废弃)

  英文解释：Initial size of permanent generation(in bytes)

  中文释义：永久代初始大小

  使用方法：-XX:PermSize=128m

- MaxPermSize：(JDK1.8 以后已废弃)

  英文解释：Maximum size of permanent generation(in bytes)

  中文释义：永久代最大值

  使用方法：-XX:MaxPermSize=256m

- MetaspaceSize：(JDK1.8 以后用于替换 PermSize)

  英文解释：Initial size of Metaspaces (in bytes)

  中文释义：元数据区初始大小

  使用方法：-XX:MetaspaceSize=128m

- MaxMetaspaceSize：(JDK1.8 以后用于替换 MaxPermSize)

  英文解释：Maximum size of Metaspaces (in bytes)

  中文释义：元数据区最大值，一旦达到全局限制，JVM 会自动增加它，为了防止出现不稳定情况（如虚拟机会耗尽所有可用的系统内存）尽量配置

  使用方法：-XX:MaxMetaspaceSize=256m

- Xss

  英文解释：Thread Stack Size(in Kbytes)

  中文释义：线程栈最大值

  使用方法：-Xss256k 或 -XX:ThreadStackSize=256k

- MaxDirectMemorySize

  英文解释：Maximum total size of NIO direct-buffer allocations

  中文释义：最大直接内存（堆外）大小

  使用方法：-XX:MaxDirectMemorySize=256m

## GC 策略相关

通过这些参数可以对 JVM 的 GC 性能进行调优

- NewRatio ：将新对象预留在新生代，由于 Full GC 的成本远高于 Minor GC，比较明智的做法是尽可能将对象分配在新生代，实际项目中根据 GC 日志分析新生代空间大小分配是否合理，适当通过“-Xmn”命令调节新生代大小，最大限度降低新对象直接进入老年代的情况

  英文解释：Ratio of old/new generation sizes

  中文释义：老年代和新生代的比值

  使用方法：-XX:NewRatio=2

  使用经验：假如设为 2，则表示老年代最大内存占堆最大内存的 2/3，新生代则为 1/3。如果设置了 Xmn 或者 NewSize/MaxNewSize，那么 NewRatio 配置无效

- SurvivorRatio

  英文解释：Rato of eden/survivor space size

  中文释义：新生代中 eden 区和 survivor 区的比值

  使用方法：-XX:SurvivorRatio=6

  使用经验：假如设为 6，则表示每个 survivor 区跟 eden 区的比值为 1:6,每个 survivor 区占新生代的八分之一

- PretenureSizeThreshold

  英文解释：Maximum size in bytes of objects allocated in DefNew generation;zero means no maximum

  中文释义：可以在新生代直接分配的对象最大值，0 表示没有最大值

  使用方法：-XX:PretenureSizeThreshold=1000000

  使用经验：设置该参数，可以使大于这个值的对象直接在老年代分配，避免在 Eden 区和 Survivor 区发生大量的内存复制，该参数只对 Serial 和 ParNew 收集器有效，Parallel Scavenge 并不认识该参数

- MaxTenuringThreshold

  英文解释：Maximum value fo tenuring threshold

  中文释义：年轻代最大年龄

  使用方法：-XX:MaxTenuringThreshold=10

  使用经验：每个对象在坚持过一次 Minor GC 之后，年龄就增加 1，当超过这个参数值时就进入老年代，最大支持 15

- UseSerialGC

  英文解释：Use the Serial garbage collector

  中文释义：年轻代使用 Serial 垃圾收集器

  使用方法：开启 -XX:+UseSerialGC

  使用经验：不推荐使用，性能太差，老年代将会使用 SerialOld 垃圾收集器

- UseParNewGC

  英文解释：Use parallel threads in the new generation

  中文释义：年轻代使用 ParNew 垃圾收集器

  使用方法：开启 -XX:+UseParNewGC

- ParallelGCThreads

  英文解释：Number of parallel threads parallel gc will use

  中文释义：并行执行 gc 的线程数

  使用方法：-XX:ParallelGCThreads=16

- UseParallelGC

  英文解释：Use the Parallel Scavenge garbage collector

  中文释义：年轻代使用 Parallel Scavenge 垃圾收集器

  使用方法：开启 -XX:+UseParallelGC

  使用经验：Linux 下 1.6,1.7,1.8 默认开启，老年代将会使用 SerialOld 垃圾收集器

- UseParallelOldGC

  英文解释：Use the Parallel Old garbage collector

  中文释义：年轻代使用 Parallel Scavenge 收集器

  使用方法：开启 -XX:+UseParallelOldGC

  使用经验：老年代将会使用 Parallel Old 收集器

- UseConcMarkSweepGC

  英文解释：Use Concurrent Mark-Sweep GC in the old generation

  中文释义：老年代使用 CMS 收集器（如果出现"Concurrent Mode Failure"，会使用 SerialOld 收集器）

  使用方法：开启 -XX:+UseConcMarkSweepGC

  使用经验：年轻代将会使用 ParNew 收集器

- CMSInitiatingOccupancyFraction

  英文解释：Percentage CMS generation occupancy to start a CMS collection cycle. A negative value means that CMSTriggerRatio is used

  中文释义：触发执行 CMS 回收的当前年代区内存占用的百分比，负值表示使用 CMSTriggerRatio 设置的值

  使用方法：-XX:CMSInitiatingOccupancyFraction=75

  使用经验：该参数需配合 UseCMSInitiatingOccupancyOnly 一起使用

- UseCMSInitiatingOccupancyOnly

  英文解释：Only use occupancy as a criterion for staring a CMS collection

  中文释义：只根据占用情况作为开始执行 CMS 收集的标准，默认关闭

  使用方法：开启 -XX:+UseCMSInitiatingOccupancyOnly

- UseCMSCompactAtFullCollection

  英文解释：Use Mark-Sweep-Compact algorithm at full collections

  中文释义：使用 CMS 执行 Full GC 时对内存进行压缩，默认关闭

  使用方法：开启 -XX:+UseCMSCompactAtFullCollection

- CMSFullGCsBeforeCompaction

  英文解释：Number of CMS full collection done before compaction if > 0

  中文释义：多少次 FGC 后进行内存压缩

  使用方法：-XX:CMSFullGCsBeforeCompaction=1

- CMSClassUnloadingEnabled

  英文解释：Whether class unloading enabled when using CMS GC

  中文释义：当使用 CMS GC 时是否启用类卸载功能，默认关闭

  使用方法：开启 -XX:+CMSClassUnloadingEnabled

- CMSParallelRemarkEnabled

  英文解释：Whether parallel remark enabled (only if ParNewGC)

  中文释义：是否启用并行标记（仅限于 ParNewGC），默认关闭

  使用方法：开启 -XX:+CMSParallelRemarkEnabled

- UseG1GC

  英文解释：Use the Garbage-First garbage collector

  中文释义：使用 G1 垃圾收集器

  使用方法：开启 -XX:+UseG1GC

- MaxGCPauseMillis

  英文解释：Adaptive size policy maximum GC pause time goal in millisecond, or (G1 Only) the maximum GC time per MMU time slice

  中文释义：自适应大小策略的最大 GC 暂停时间目标（以毫秒为单位），或（仅 G1）每个 MMU 时间片的最大 GC 时间

  使用方法：-XX:MaxGCPauseMillis=200

- DisableExplicitGC

英文解释：Ignore calls to System.gc()

中文释义：禁用 System.gc()触发 FullGC

使用方法：开启 -XX:+DisableExplicitGC

使用经验：不建议开启，如果开启了这个参数可能会导致堆外内存无法及时回收造成内存溢出

## GC 日志相关

通过这些参数可以对 JVM 的 GC 日志输出进行配置，方便分析

- Xloggc

  英文解释：GC log file

  中文释义：GC 日志文件路径

  使用方法：-Xloggc:/data/gclog/gc.log

- UseGCLogFileRotation

  英文解释：Rotate gclog files(for long running applications). It requires -Xloggc:<filename>

  中文释义：滚动 GC 日志文件，须配置 Xloggc

  使用方法：开启 -XX:+UseGCLogFileRotation

- NumberOfGCLogFiles

  英文解释：Number of gclog files in rotation(default:0,no rotation)

  中文释义：滚动 GC 日志文件数，默认 0，不滚动

  使用方法：-XX:NumberOfGCLogFiles=4

- GCLogFileSize

  英文解释：GC log file size,requires UseGCLogFileRotation. Set to 0 to only trigger rotation via jcmd

  中文释义：GC 文件滚动大小，需配置 UseGCLogFileRotation，设置为 0 表示仅通过 jcmd 命令触发

  使用方法：-XX:GCLogFileSize=100k

- PrintGCDetails

  英文解释：Print more details at garbage collection

  中文释义：GC 时打印更多详细信息，默认关闭

  使用方法：开启 -XX:+PrintGCDetails

  使用经验：可以通过 jinfo -flag [+|-]PrintGCDetails <pid> 或 jinfo -flag PrintGCDetails=<value> <pid> 来动态开启或设置值

- PrintGCDateStamps

  英文解释：Print date stamps at garbage collection

  中文释义：GC 时打印时间戳信息，默认关闭

  使用方法：开启 -XX:+PrintGCDateStamps

  使用经验：可以通过 jinfo -flag [+|-]PrintGCDateStamps <pid> 或 jinfo -flag PrintGCDateStamps=<value> <pid> 来动态开启或设置值

- PrintTenuringDistribution

  英文解释：Print tenuring age information

  中文释义：打印存活实例年龄信息，默认关闭

  使用方法：开启 -XX:+PrintTenuringDistribution

- PrintGCApplicationStoppedTime

  英文解释：Print the time of application has been stopped

  中文释义：打印应用暂停时间，默认关闭

  使用方法：开启 -XX:+PrintGCApplicationStoppedTime

- PrintHeapAtGC

  英文解释：Print heap layout before and after each GC

  中文释义：GC 前后打印堆区使用信息，默认关闭

  使用方法：开启 -XX:+PrintHeapAtGC

## 异常相关

通过这些参数可以在 JVM 异常情况下执行某些操作，以保留现场做分析用

- HeapDumpOnOutOfMemoryError

  英文解释：Dump heap to file when java.lang.OutOfMemoryError is thrown

  中文释义：抛出内存溢出错误时导出堆信息到指定文件，默认关闭

  使用方法：开启 -XX:+HeapDumpOnOutOfMemoryError

  使用经验：可以通过 jinfo -flag [+|-]HeapDumpOnOutOfMemoryError <pid> 或 jinfo -flag HeapDumpOnOutOfMemoryError=<value> <pid> 来动态开启或设置值

- HeapDumpPath

  英文解释：When HeapDumpOnOutOfMemoryError is on, the path(filename or directory) of the dump file(defaults to java_pid<pid>.hprof in the working directory)

  中文释义：当 HeapDumpOnOutOfMemoryError 开启的时候，dump 文件的保存路径，默认为工作目录下的 java_pid<pid>.hprof 文件

  使用方法：-XX:HeapDumpPath=/data/dump/jvm.dump

  使用经验：除非必要，建议不设置

- OmitStackTraceInFastThrow

  英文解释：Omit backtraces for some ‘hot’ exceptions in optimized code

  中文释义：在优化代码里面忽略热点异常回溯

  使用方法：关闭 -XX:-OmitStackTraceInFastThrow

  使用经验：某些热点异常抛的太多的话，JVM 默认会做优化，会使用 JVM 初始化的时候创建的异常代替实际的异常，这些异常是没有异常栈信息的，不方便定位问题，如果有碰到这种情况，可以考虑关闭这个配置

## 问题定位及优化相关

通过这些参数可以对 JVM 进行性能优化或者排查定位问题

- server

  英文解释：server mode

  中文释义：使用服务端模式

  使用方法：-server

- TieredCompilation

  英文解释：Enable tiered compilation

  中文释义：启用多层编译

  使用方法：开启 -XX:+TieredCompilation 关闭 -XX:-TieredCompilation

  使用经验：java 1.8 默认开启分层编译，该参数无效

- NativeMemoryTracking

  英文解释：Native memory tracking options

  中文释义：开启本机内存追踪

  使用方法：开启详细信息 -XX:NativeMemoryTracking=detail 开启概要信息 -XX:NativeMemoryTracking=summary

  使用经验：开启的话，大概会增加 5%-10%的性能消耗

- UnlockDiagnosticVMOptions

  英文解释：Enable normal processing of flags

  中文释义：解锁对 JVM 进行诊断的选项参数，默认关闭

  使用方法：开启 -XX:+UnlockDiagnosticVMOptions

- PrintNMTStatistics

  英文解释：Print native memory tracking summary data if it is on

  中文释义：在 jvm shutdown 的时候输出整体的 native memory 统计，默认关闭

  使用方法：开启 -XX:+PrintNMTStatistics

  使用经验：必须配合参数-XX:+UnlockDiagnosticVMOptions 使用，并且只能加在其后才能生效

- UseAdaptiveSizePolicy

  英文解释：Use adaptive generation sizing policies

  中文释义：使用自适应分代内存策略

  使用方法：开启 -XX:+UseAdaptiveSizePolicy 关闭 -XX:-UseAdaptiveSizePolicy

  使用经验：1.7 以后默认会开启该参数，如果使用 CMS 回收算法，则会关闭该参数，该参数开启以后会使 SurvivorRatio 参数失效，如果显示指定了 SurvivorRatio，需要关闭该参数
