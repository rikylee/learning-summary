# RocketMQ延时消息的使用和延时级别的配置

RocketMQ 支持定时消息，但是不支持任意时间精度，仅支持特定的 level，例如定时 5s,10s,1m等。其中level=0级表示不延时，level=1 表示 1 级延时，level=2 表示2级延时，以此类推。

## 服务端配置

需要在服务器端的属性配置文件中`$ROCKETMQ_PATH/conf/broker.conf`加入以下行，用于设置延时级别：

```conf
messageDelayLevel=1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
```

上面配置行描述了各级别与延时时间的对应映射关系，详情如下：

- 这个配置项配置了从1级开始，各级延时的时间，可以修改这个指定级别的延时时间，如想要设置消息延时10s,设置level为3即可

- 时间单位支持：s、m、h、d，分别表示秒、分、时、天

- 默认值就是上面声明的，可手工调整，一般来说默认值已够用，不建议修改这个值

## 发送延时消息

发送延时消息只需要在客户端待发送的消息(com.alibaba.rocketmq.common.message.Message)中设置延时级别即可

```
Message msg = new Message(topic, tags, keys, body);
msg.setDelayTimeLevel(3);
SendResult sendResult = getMQProducer().send(msg);
```

如果引入了rocketmq-spring-boot-starter，默认使用RocketMQTemplate进行消息的发送，此时代码需要做一些调整

```
DefaultMQProducer producer = rocketMQTemplate.getProducer();
Message msg = new Message(topic, tags, keys, body);
msg.setDelayTimeLevel(3);
SendResult sendResult = producer.send(msg);
```
