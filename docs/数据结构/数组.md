# 数组

## 定义

数组（Array）是一种线性表（线性表就是数据排成像一条线一样的结构，每个线性表上的数据最多只有前和后两个方向，除了数组，链表、队
列、栈等也是线性表结构）数据结构，它用一组连续的内存空间，来存储一组具有相同类型的数据。

## 特点

- 随机访问（通过首地址和元素序号可在时间O(1)内找到元素）
- 插入和删除需要移动大量元素
- 存储密度高，每个结点只存储数据元素

## 插入和删除复杂度

- 插入元素的时间复杂度

  假设数组的长度为n，如果我们需要将一个数据插入到数组中的第m个位置。为了把第m个位置腾出来，给新来的数据，我们需要将第m～n这部分的元素都顺序地往后挪一位。如果在数组的末尾插入元素，那就不需要移动数据了，这时的时间复杂度为 O(1) ，但如果在数组的开头插入元素，那所有的数据都需要依次往后移动一位，所以最坏时间复杂度是 O(n)。因为我们在每个位置插入元素的概率是一样的，所以平均情况时间复杂度为 (1+2+…n)/n=O(n)。

  如果要求数组的的元素必须是有序的，则必须按照如上方式去移动m之后的位置，但是如果数组内的数据不要求有序，为了避免大规模的数据搬移，我们还有一个简单的办法就是，直接将第m位的数据搬移到数组元素的最后，把新的元素直接放入第m个位置方式,在这个特定场景下，在第m个位置插入一个元素的时间复杂度就会降为 O(1)。

- 删除元素的时间复杂度

  如果我们要删除第m个位置的数据，为了内存的连续性，也需要搬移数据，不然中间就会出现空洞，内存就不连续了。和插入类似，如果删除数组末尾的数据，则最好情况时间复杂度为 O(1) ；如果删除开头的数据，则最坏情况时间复杂度为 O(n) ；平均情况时间复杂度也为 O(n)。

## 代码实现

```java

public class MyArray {
  private int[] data;
  transient int size; //数组的长度
  transient int count; //实际存储元素的个数

  //构造方法，定义数组大小
  public MyArray(int capacity) {
  data = new int[capacity];
  this.size = capacity;
  this.count = 0; //一开始一个数都没有存所以为0
  }

  //根据索引，找到数据中的元素并返回
  public int get(int index) {

    if(index<0 || index>=count) return -1; // 越界或者下标大于等于实际存储的个数返回-1，表示未找到

    return data[index];
  }


  public boolean insert(int value) {
    //数组空间已满
      if(count==size) {
        System.out.println("数组空间已满，无法插入");
        return false;
      }

      data[count++]=value;

      return true;
  }
 //插入元素
  public boolean insert(int index, int value) {

    //数组空间已满
    if(count==size) {
    System.out.println("数组空间已满，无法插入");
    return false;
    }
    if(index<0 || index>count) {
      System.out.println("位置不合法");
            return false;
    }
    // index以及之后的元素向后移动
    for(int i=count;i>index;i--) {
    data[i]=data[i-1];
    }
    data[index]=value;
    count ++;

    return true;
  }

    public boolean delete(int index){

     if(index<0 || index>=count) return false;

     //index之后的元素向前移动
     for(int i=index+1;i<count;i++) {
      data[i-1]=data[i];
     }
     count--;

     return true;
    }

  public void printAll() {
      for (int i = 0; i < count; ++i) {
          System.out.print(data[i] + " ");
      }
      System.out.println();
  }

  public static void main(String[] args) {
    MyArray  arr = new MyArray(10);

    arr.insert(5);
    arr.insert(3);
    arr.insert(2);
    arr.insert(1);
    arr.insert(0);

    arr.printAll();

    arr.insert(1, 4);
    arr.insert(0, 6);

    arr.printAll();

    System.out.println(arr.count);

    arr.delete(3);

    arr.printAll();

    System.out.println(arr.get(4));
 }
}

```
